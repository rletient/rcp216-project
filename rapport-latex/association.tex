\documentclass[11pt]{article}
\usepackage{url} % pour la gestion des urls
\usepackage[utf8]{inputenc} % un package
\usepackage[T1]{fontenc}      % un second package
\usepackage[french]{babel}  % un troisième package
\usepackage{hyperref}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{color}
\usepackage{csvsimple}
\usepackage{courier}
\usepackage{float}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage{lmodern} % Pour changer le pack de police
\usepackage{graphicx}
\usepackage{lscape}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage{epstopdf}
\usepackage{lscape}

\author{Rémy Letient}
\date{2017-2018} 
\title{UE RCP216 - \emph{Associations Reconnues d’Utilité Publique}}

\begin{document}
\definecolor{mygray}{rgb}{0.95,0.95,0.95}

\maketitle
\begin{abstract}
Ce projet a pour but de réaliser une classification automatique de texte sur une base d'analyse textuelle et de décrire la classification initiale via des termes pertinents.
\bigbreak
Nous aborderons l'approche \emph{Word2Vec} et la matrice \emph{TF-IDF}.
\end{abstract}

\newpage

\tableofcontents

\newpage

\section{Le sujet}

\emph{Associations Reconnues d’Utilités Publiques : à partir des données ouvertes de \url{https://www.data.gouv.fr/fr/datasets/associations-reconnues-d-utilite-publique/}, appliquer une classification automatique à partir du contenu de l’attribut textuel (colonne Objet). Chercher les termes issus de ces descriptions textuelles (colonne Objet) qui sont les plus pertinents pour chaque catégorie déclarée (colonne Catégorie).}
\bigbreak
Nous avons ici 2 problématiques, l'une concernant une problématique de classification non supervisée de documents (nos associations), et l'autre un problème de recherche de termes pertinents. 
\bigbreak
Pour la classification, nous mettrons en œuvre un algorithme de type K-Means qui sous-entend 2 pré-requis : 
\begin{itemize}
\item Avoir un représentation vectorielle de nos données textuelles. Pour se faire, nous utiliserons une représentation basée sur la méthode \emph{Word2Vec}
\item Déterminer le "bon" nombre de classes que nous souhaitons constituées. Pour se faire, nous nous baserons sur le calcul du coefficient \emph{silhouette} en faisant varier \emph{k} de 1 à 100.
\end{itemize}
\bigbreak
Pour la recherche de termes pertinents, nous calculerons une matrice \emph{TF-IDF} pour l'ensemble des termes présents dans nos données, nous permettant de déterminer les termes les plus pertinents pour chacune des catégories d'associations.
\bigbreak
Dans le cadre de ce projet, j'ai avons aussi développé une application permettant la visualisation de la classification automatique basée sur une approche de type \emph{Treemap} et \emph{Nuage de mots}.

\section{Les données}

Il s'agit d'un fichier de type \emph{OpenOffice Calc} contenant 2 073 associations réparties en 30 catégories. 2 colonnes de ce fichier nous intéressent particulièrement, la colonne \emph{catégorie} et la colonne \emph{objet}. Toutes les informations sont écrites en Français. \\
Le contenu n'est pas homogène et il existe des fautes de frappes notamment pour la saisie de la \emph{catégorie}.

\subsection{Catégorie}
Les catégories définissent des domaines d'activités plus ou moins vastes : santé, ancien combattant ...\\
La figure~\ref{data:categorie} illustre la répartition des associations dans les différentes catégories\footnote{Programme : \url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part1ExtractCategorie.java}}. Nous pouvons constater que la répartition n'est pas homogène entre les catégories. 

	\begin{figure}[H]		
	  	\centering
		\setkeys{Gin}{width=1\textwidth}
	  	\includegraphics{attachments/repartition-association.png}
	  	\caption{Répartition des associations d'utilité publique en catégorie}
	  	\label{data:categorie}
	 \end{figure}
	 
\subsection{Objet}
Il s'agit de descriptions plus ou moins courtes pour une association donnée, le style d'écriture peut varier. Certains descriptifs utilisent des abréviations. Toutes sont rédigées en Français, en utilisant un registre de vocabulaire commun. Voici quelques exemples : 
\bigbreak
\emph{
"Lutter contre la prostitution"
}
\bigbreak

\emph{
"ADM, basée à Grenoble, a mené depuis 1999 plusieurs projets au Vietnam. L'ass. propose des actions de soutien simple. Ses actions consistent princip. En  des travaux de rénov. et d'équip. de struct. médic. accompag. de missions de formation/place."}
\bigbreak

\emph{
"- encourager et développer le goût de la navigation, de l’art de la navigation à voile et des exercices nautiques, \\
- instruire les jeunes dans l’art de la navigation\\
- subventionner les œuvres de mer,\\
- promouvoir culture et éthique maritimes,\\
- œuvrer pour l’écologie maritime et la protection des mers et océans,\\
- encourager et s'associer à l’étude de toutes questions se rapportant à la marine,\\
- contribuer à la sauvegarde du patrimoine maritime et des bateaux de tradition, et d’encourager les progrès dans la construction navale\\
- soutenir la compétition à la voile et le mécénat sportif dans le respect des valeurs portées par l’association,\\
- s’associer et encourager toute activité locale ou internationale en relation avec ces buts,\\
- gérer directement ou indirectement le plan d’eau par délégation de service public ou sous tout autre forme."}
\bigbreak
Le dernier exemple est intéressant car cette association a été classée dans la catégorie \emph{éducation formation}, alors qu'à la lecture de la description plusieurs autres catégories peuvent nous venir à l'esprit comme \emph{sport} ou encore \emph{environnement}.

\section{Classification automatique des associations}
Pour permettre la classification automatiques des données, j'ai mis en œuvre la méthode des K-Means. Pour se faire, j'avais besoin de créer une représentation vectorielle des descriptions des associations. C'est dans ce contexte que j'ai choisi l'approche \emph{Word2Vec} car cette approche s'avère adaptée et efficace pour la représentation de textes courts que sont mes descriptions d'associations. Elle apporte un raffinement sémantique qui permet des rapprochements entre termes orthographiquement différents mais dont le sens est proche en se basant sur un modèle et non une ontologie de type WordNet\footnote{\url{https://wordnet.princeton.edu/}}.

\subsection{Représentation vectorielle des données textuelles avec Word2Vec}
La base de cette approche est l'utilisation d'un modèle entraîné à partir d'un corpus. Il nous faut donc avant même de penser à créer le modèle, partir à la recherche d'un corpus respectant les contraintes suivantes :
\begin{description}
\item [Langue :] Français
\item [Adapté à notre projet :] l'étude des descriptions nous permet de dire que nous sommes sur un lexique généraliste
\item [Exhaustif :] pour permettre une précision maximale dans les rapprochements sémantiques des termes analysés
\end{description}
\bigbreak
C'est avant tout la recherche d'exhaustivité qui m'a poussé à rechercher en premier lieu des modèles pré-existant plutôt que de tenter d'en générer un par mes propres moyens à partir de mes associations. Les recherches m'ont amenées à trouver les travaux de Jean-Philippe Fauconnier\footnote{\url{http://fauconnier.github.io/}} en la matière, que j'ai utilisé et que je remercie donc.

\subsubsection{Le choix du modèle}
Les modèles de Philippe Fauconnier respectaient mes critères, se basant, pour le corpus, sur frWaC\footnote{\url{http://wacky.sslmit.unibo.it/doku.php?id=corpora}} avec plus 1.6 milliards de mots. Au tour de cette base, il a généré plusieurs modèles faisant varier l'approche sur l'entité de travail : \emph{mot brut}, \emph{lemme plus position grammatical}, ou \emph{morceau de phrase} ; la dimension du vecteur attachée à l'entité de travail (de 200 à 1000) ; ou encore le type d’entraînement du modèle : \emph{cbow} pour \emph{continuous bag-of-word} (il s'agit de prédire un mot en fonction d'un contexte donné) et \emph{skip} pour \emph{skip-gram} (il s'agit de prédire un contexte étant donné un mot). 
\bigbreak
Dès le départ, mon choix s'est porté sur les modèles utilisant l'approche \emph{lemme plus position grammaticale} pour l'entité de travail, qui sera au finale mon critère d'entrée pour exploiter le modèle. Cette approche permet de factoriser les termes sous leur forme la plus simple (\emph{retrouverais} nous donne \emph{retrouver}) en faisant l'impasse sur des informations sémantiques qui ne nous sont pas utiles, comme : le genre, le nombre et la forme pronominale des verbes. Il s'agit ici d'optimiser le travail d'apprentissage autour d'un seul terme, \emph{retrouver},  plutôt que d'éparpiller ce travail sur l'ensemble des formes que peut prendre ce terme \emph{retrouver}. Pour le type d’entraînement, j'ai opté pour \emph{skip-gram} qui est adapté à notre cas. Et pour finir, concernant la dimension, j'ai choisi la dimension la plus important (plus de précision) que pouvait supporter ma machine à savoir 700. 
\bigbreak
La difficulté technique sur cette partie a été d'exploiter le format binaire des modèles de Philippe Fauconnier au sein de Spark. Pour cette partie, je remercie le projet \emph{Medallia}\footnote{\url{https://github.com/medallia/Word2VecJava}} sur lequel j'ai fondé ma propre implémentation de lecture du modèle \emph{Word2VecModelWithEncodingLoader}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/com/medallia/word2vec/Word2VecModelWithEncodingLoader.java}}. Cette classe associée à la classe \emph{Word2VecModelLoader}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/groovy/org/homework/rcp216/loader/word2vec/Word2VecModelLoader.groovy}} permet d'utiliser un modèle binaire directement sous la forme d'un modèle \emph{Word2Vec} Spark. Elle a aussi comme responsabilité de gérer l'encoding des caractères accentués présents dans le format binaire.

\subsubsection{Lemme et étiquetage grammatical avec OpenNLP}
Avoir un modèle dont les entrées sont des  \emph{lemme plus position grammaticale} nécessitent en contrepartie d'avoir un processus de lemmatisation et d'étiquetage grammatical pour nos données en Français, afin de pouvoir l'utiliser au sein de notre projet. Pour le traitement de cette problématique, je me suis appuyé sur le projet OpenNLP\footnote{\url{https://opennlp.apache.org/docs/1.8.4/manual/opennlp.html}}. J'ai donc créé la classe \emph{DecomposerByLemmaPos}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/opennlp/DecomposerByLemmaPos.java}} pour son utilisation dans le cadre du projet dont le programme \emph{Part2AssociationToLemma}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part2AssociationToLemma.java}} en est la première utilisation. Ce programme a pour but de convertir chacune des descriptions en une liste de termes sous la forme \emph{lemme plus position grammatical} , par exemple :
\begin{description}
\item[Description :] Enrichir les collections du musée et assurer un plus large accès et une plus large diffusion.
\item[Termes :] enrichir\_v collection\_n musée\_n assurer\_v plus\_adv large\_a accès\_n plus\_adv large\_a diffusion\_n
\end{description}
\bigbreak
Une des difficultés de cette étape a été de faire converger l'étiquetage grammaticale en tant que tel d'OpenNLP vers l'étiquetage qui a été utilisé par Philippe Fauconnier dans son modèle.  Mais aussi de réaliser un prétraitement de la donnée (suppression de ponctuation ...) avant de réaliser cet étiquetage via OpenNLP afin que celui-ci soit pertinent. Ce travail est illustré par la classe PersonnalLemmeDictionnary~\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/groovy/org/homework/rcp216/opennlp/PersonnalLemmeDictionnary.groovy}}.

\subsubsection{Evaluation du modèle et de l'utilisation d'OpenNLP}

Une fois qu'on a rendu nos données utilisables avec le modèle \emph{Word2Vec}, j'ai réalisé une évaluation du modèle pour déterminer les termes que le modèle ne connaissait pas. Cette évaluation a été réalisée via le programme \emph{Part3ChallengeModel}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part3ChallengeModel.java}}.
\bigbreak
Nous avons 4 329 termes uniques dans nos données, l'évaluation révèle que plus de 600 d'entre eux ne sont pas connus. Dans les termes inconnus, on y trouve des nombres correspondant à des années ou des âges, des mots techniques comme \emph{actinothérapie}, des abréviations de mots utilisés comme \emph{accompag, cptes} ainsi que des mots inconnus à cause de leur mauvais étiquetage grammatical comme \emph{accident\_v}, sans parler d'entité nommée (nom de peintre, d'écrivain ...).
\bigbreak
Il s'agit de l'axe d'amélioration principale de ce projet en améliorant l'étiquetage grammatical et la complétude du modèle. 

\subsubsection{Création de la représentation vectorielle}

Le programme \emph{Part4AssociationToW2Vec}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part4AssociationToW2Vec.java}} illustre l'algorithme mis en place pour obtenir une représentation vectorielle pour chacune de nos associations. Il s'agit simplement de faire la moyenne des vecteurs que représente chacun des termes constituant sa description. Une fois la représentation vectorielle réalisée, j'ai créé une \emph{User-defined functions (UDFs)} permettant de traduire cette représentation vectorielle en mots via ma classe \emph{W2VToWord}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/groovy/org/homework/rcp216/udf/W2VToWord.groovy}} pour juger de la pertinence de l'approche, dont voici quelques exemples :

\bigbreak
\begin{description}
\item[Objet :] Aider moralement et matériellement les musiciens en cas de maladie grave ou de détresse notoire, plus spécialement les vieux musiciens et exceptionnellement leurs ascendants, descendants, et conjoints selon les ressources de l'association
\item[Mots :] aider\_n moralement\_n matériellement\_adv musicien\_n cas\_n maladie\_n grave\_a détresse\_n notoire\_a plus\_adv spécialement\_adv vieux\_a musicien\_n exceptionnellement\_adv ascendant\_n conjoint\_n ressource\_n association\_n
\item[Word2Vec :] conjoint, ascendant, famille, cas, agir, conjoints
\end{description}
\bigbreak
\begin{description}
\item[Objet :] Sauvegarde de tout élément du patrimoine naturel et bâti du département de l’Eure
\item[Mots :] sauvegarde\_n élément\_n patrimoine\_n naturel\_a bâti\_a département\_n eure\
\item[Word2Vec :] eure, patrimoine, département, sauvegarde, naturel, préservation, eure-et-loir, départemental
\end{description}

\bigbreak
La difficulté technique ici a été la gestion de la mémoire pour éviter le \emph{out of memory} avec tout de même 10 Go de RAM à disposition, 2 approches ont ainsi été codées : 
\begin{itemize}
\item Ensembliste\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/model/Association.java\#L173}} : Basée sur l'utilisation des Dataframes Spark comme depuis le début du projet. Elle se fonde sur une fonction d’agrégation que j'ai codé spécialement pour faire la moyenne non pondéré des vecteurs, à savoir : \emph{VectorMeanUDAF}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/groovy/org/homework/rcp216/udf/VectorMeanUDAF.groovy}}. Cette méthode a été sujette à des \emph{out of memory} lors de l'utilisation du modèle \emph{Word2Vec} à 700 dimensions.
\item Incrémentale\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/model/Association.java\#L136}} : Basée sur l'usage des RDD, que j'ai finalement utilisée
\end{itemize}

\subsection{A la recherche du bon \emph{k}}

Notre premier pré-requis étant atteint, avoir des représentations vectorielles de nos associations, il reste à rechercher le meilleur \emph{k}. Pour se faire, j'ai codé le programme \emph{Part5KMeans}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part5KMeans.java}}. Son but est de tester tous les \emph{k} possible de 1 à 100, de les évaluer en calculant le coefficient silhouette et d'effectuer une synthèse (7h de traitement). Pour la création de chacun des clusters, j'ai fixé le seuil de convergence à 200, afin d'obtenir une précision importante dans leur établissement.
\bigbreak
Le coefficient \emph{silhouette} est un coefficient compris entre -1 et 1, 1 signifiant que les classes sont parfaitement formées. Le tableau~\ref{table:kmean:silhouette} en est un extrait, suite à son étude j'ai choisi un \emph{k} à 41. Il est à la fois l'un des coefficients \emph{silhouette} les plus important de mon expérimentation, et le proche du nombre de catégorie initiale (30).

\shorthandoff{;}
	\begin{figure}[H]
		\setkeys{Gin}{width=0.5\textwidth}	  	  	  	  
		\centering		
		\csvautotabular[separator=semicolon]{attachments/silhouette.csv}
		\caption{Extrait du bilan des itérations de \emph{k} - 10 premiers résultats par ordre décroissant - coef. \emph{silhouette}}
    	\label{table:kmean:silhouette}
	 \end{figure}	 
\shorthandon{;}

\subsection{Visualisation}

Avant de visualiser, il a fallu créer nos 41 classes et les affecter aux données. C'est le rôle du programme \emph{Part6AssociationClassification}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part6AssociationClassification.java}}. Le programme  \emph{VisuConvertCSVToJSON}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/groovy/org/homework/rcp216/VisuConvertCSVToJSON.groovy}} a permis de convertir la sortie csv du précédent programme en format json utilisé par mon programme de visualisation permettant d'obtenir les figures \ref{fig:classification:treemap:w2v} et \ref{fig:classification:treemap:categorie}.
\bigbreak

La figure~\ref{fig:classification:treemap:w2v} met en avant l'approche \emph{Word2Vec} en illustrant les membres du cluster dont les identifiants sont en rouge, par les termes "proches" de la moyenne de l'ensemble des vecteurs des associations membres. Les termes sont classés de haut en bas selon leur précision. Si on prend le membre 27, le terme le plus "proche" est le terme \emph{association} suivi de \emph{action}. Les termes sont sujets à une opacité égale à leur précision, pour les box elle est égale à la précision du premier terme.
\bigbreak
Si on prend le membre 24 : le terme \emph{tuberculose} a un précision de 0.77, alors que le terme \emph{maladie} 0.68. 
\bigbreak
Ainsi, on peut observer qu'il n'y pas forcément de lien entre la taille du membre, c'est à dire le nombre d'associations qui le constitue, et la pertinence des termes qui le représente : membre 0, 136 associations, précision du premier terme : 0.7 et membre 39, 53 associations, précision du premier terme : 0.56. 
\bigbreak
Classe typescript responsable de cet affichage : \emph{RowWordCloudDirective}\footnote{\url{https://gitlab.com/rletient/rcp216-visu/blob/master/src/app/word-cloud/row-word-cloud.directive.ts}}


	\begin{figure}[H]
		\setkeys{Gin}{width=1\textwidth}
		\includegraphics{attachments/classification_auto.PNG}
		\caption{Classification automatique basée sur les K-means, avec un \emph{k} de 41.}
		\label{fig:classification:treemap:w2v}
	\end{figure}

La figure~\ref{fig:classification:treemap:categorie} utilise un \emph{Treemap} au sein de chacun des membres pour illustrer la répartition par catégorie initiale des associations qui la composent. \\
Classe typescript responsable de cet affichage : \emph{PartitionDirective}\footnote{\url{https://gitlab.com/rletient/rcp216-visu/blob/master/src/app/word-cloud/partition.directive.ts}}
	
	\begin{figure}[H]
		\setkeys{Gin}{width=1\textwidth}
		\includegraphics{attachments/classification_auto_par_categorie.PNG}
		\caption{Classification automatique répartition des catégories initiales}
		\label{fig:classification:treemap:categorie}
	\end{figure}

\subsection{Bilan}

Sur cette partie, ma conclusion se base sur l'analyse des figures \ref{fig:classification:treemap:w2v}, \ref{fig:classification:treemap:categorie} et du ficher \emph{Part6AssociationClassification.objet.csv} par le programme \emph{Part6AssociationClassification}\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part6AssociationClassification.java}}.
\bigbreak
Cette classification montre qu'il y a encore des progrès à faire même si une classification non supervisée est toujours difficile à évaluer. On peut constater qu'il existe des classes trop généralistes regroupant trop de catégories différentes comme la classe 0. Les catégories initiales ont très peu affectées la classification automatique, contrairement à mes premières attentes. Il faut partir à la recherche d'éléments encore plus discriminants pour mieux pouvoir caractériser les classes. Le seul axe de travail avait été de ne prendre en considération que les noms, les verbes et les adjectifs des descriptions d'association en leur accordant le même poids. 
\bigbreak
Un axe de travail serait de faire un premier résumé du descriptif d'une association par ces termes les plus pertinents (définition d'un seuil) via l'établissement d'une matrice de type \emph{TF-IDF} de l'ensemble des termes. Et ensuite, appliquer le modèle \emph{Word2Vec} à ces termes pour obtenir une représentation vectorielle. 
\bigbreak
Sinon, sur le fond, on peut aussi se poser la question de comment définir la notion de proximité entre des associations. Je pense qu'ici c'est le besoin métier qui aiderait, nous permettant de privilégier un axe de travail. Est ce que 2 associations sont proches parce qu'elles se situent dans le même département, un club de voile doit il être plus proche d'une association de protection des environnements marin ou d'un club de foot ? 
\bigbreak
Les bases techniques ont été posées, prêtes pour de future itération sur ce vaste sujet.

\newpage
\section{A la recherche de la pertinence}

Concernant la seconde problématique du projet, la recherche de termes pertinents pour illustrer les catégories présentes, ayant plongé dans l'approche \emph{Word2Vec} j'ai cherché à l'appliquer puis pour obtenir plus de précision, j'ai appliqué une méthode plus classique qui est l'approche \emph{TF-IDF}.

\subsubsection{Avec \emph{Word2Vec}}
	La première approche, pour rechercher des termes pertinents, a été de continuer à exploiter l'approche \emph{Word2Vec}. Il s'agissait d'obtenir une représentation vectorielle d'une catégorie au travers de la moyenne non pondérée de l'ensemble des vecteurs des associations qui la composent. Une fois la représentation vectorielle établie, il s'agissait d'en extraire via le modèle \emph{Word2Vec} les mots les plus proches.
\bigbreak	
	Le programme \emph{Part7CategorieToW2V}~\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part7CategorieToW2V.java}} a été créé pour fournir un fichier csv et le programme \emph{VisuCategorieCSVToJSON}~\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/groovy/org/homework/rcp216/VisuCategorieCSVToJSON.groovy}} a été créé pour fournir une représentation JSON de l'ensemble au programme de visualisation. 
\bigbreak
	La figure~\ref{fig:categorie:pertinence:w2v} en est le résultat. Pour rendre celui-ci le plus lisible possible, je n'ai pas rendu la taille des box proportionnelles aux nombres d'associations qu'elles contiennent, toutes les box ont la même surface.  
\bigbreak	
	Globalement, l'approche donne des résultats cohérents avec la catégorie associée, on peut tout de même déplorer des exceptions comme pour la catégorie \emph{clubs loisirs} avec les termes : \emph{divers, nombreux, différent, développer, permettre, ensemble, contribuer} ; qui restent un peu trop vague. C'est ce type de constat qui m'a motivé à rechercher une autre méthode.
	\begin{figure}[H]
		\setkeys{Gin}{width=1\textwidth}
		\includegraphics{attachments/categorie_w2v.PNG}
		\caption{Catégorie - Termes pertinents - \emph{Word2Vec}}
		\label{fig:categorie:pertinence:w2v}
	\end{figure}
	
\subsubsection{Avec \emph{TF-IDF}}
	Bien que l'approche \emph{Word2Vec} soit globalement satisfaisante, malheureusement quelques catégories étaient représentées par des termes trop vagues. J'ai donc voulu tester une autre méthode, utilisée pour la constitution des index de recherche pour qualifier les termes selon leurs caractères représentatif d'un document au sein d'une collection. Cela consiste à calculer une matrice des \emph{TF-IDF} pour l'ensemble des termes de nos associations, qui représentent nos documents. Une fois cette matrice calculée, j'ai fait un regroupement de tous les termes par catégorie via les associations qui la composent. Ensuite, cela a consisté à extraire les 8 termes dont l'indice \emph{TF-IDF} étaient les plus importants. 
\bigbreak
Le programme \emph{Part8CategorieTFIdf}~\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/java/org/homework/rcp216/Part8CategorieTFIdf.java}} illustre cette démarche, la figure~\ref{fig:categorie:pertinence:tfidf} le résultat. Comme pour la figure précédente, toutes les catégories ont la même surface. Note : il n'y a que quatre termes dans la catégorie \emph{logement} car celle-ci et constituée d'une seule association dont le description est : \emph{"Amélioration des logements insalubres ou leur suppression"}. Comme il n'y a que 4 termes,  il y a plus de place, le programme a cherché à tout utiliser, la taille de font est donc plus importante. C'est un fait notable qui nous rappelle que via l'approche \emph{Word2Vec} cela n'était pas visible car les termes étaient au final extrait du modèle et non de notre corpus.
\bigbreak
Nous pouvons constater la présence de moins de verbe, le gain en pertinence n'est pas évident : 
\begin{description}
\item [Word2Vec, clubs loisirs] :  \emph{divers, nombreux, différent, développer, permettre, ensemble, contribuer}
\item [TF-IDF, clubs loisirs] : \emph{tendre, espace, émetteur, consolider, armé, choisir, fondée, ayant}
\end{description}

	\begin{figure}[H]
		\setkeys{Gin}{width=1\textwidth}
		\includegraphics{attachments/categorie_tfidf.PNG}
		\caption{Catégorie - Termes pertinents - \emph{TF-IDF}}
		\label{fig:categorie:pertinence:tfidf}
	\end{figure}

\section{Bilan}

Un projet de fouille de texte requière beaucoup d'attention à chacune des étapes qui la constitue car chacune forme un maillon. De la qualité de chacun des maillons dépend la qualité de l'ensemble du projet. Si mon outil d'étiquetage grammatical n'est pas performant, il y a de forte chance que le modèle ne connaisse pas mon verbe \emph{bonjour}. Si mon modèle n'est pas suffisamment exhaustif, il rejettera mes mots à analyser, certaines proximités souhaitées ne seront pas présentes ... 
\bigbreak
Pour moi, l'approche \emph{Word2Vec} que cela soit pour la problématique de classification automatique ou de termes descriptifs, a montré un grand potentiel, on peut entrevoir beaucoup d'améliorations et d'exploration sur la base du travail déjà effectué : faire varier les modèles (tester le modèle à 1 000 dimensions), diminuer le nombre de rejet par le modèle, ajouter un composant pour analyser les entités nommés et les remplacer par des termes plus généralistes en se basant sur un ontologie par exemple : Jules Vernes serait remplacé par écrivain. Ne le faire travailler que sur des termes pertinents.

\section{Quelques mots sur l'environnement de travail}

Concernant la partie Spark les développements ont été réalisés en Java avec l'IDE Intellij et un soupçon de Groovy~\footnote{\url{http://groovy-lang.org/}}. Le tout est sous la forme d'un projet Maven qu'il suffit d'ouvrir avec Intellij. Les programmes cités nécessitent tous en entrée un fichier de properties dont vous trouverez ma configuration ici\footnote{\url{https://gitlab.com/rletient/rcp216-project/blob/master/src/main/resources/project.properties}}. \\Le projet est disponible sous gitlab à l'adresse suivante : \url{https://gitlab.com/rletient/rcp216-project}
\bigbreak
Concernant la partie visualisation, les développements ont été réalisés en Angular4/Typescript avec l'IDE VisualCode. Pour le lancer il suffit de taper les commandes suivantes : \emph{npm install} et \emph{ng serve}. Les prérequis sont d'avoir node/npm d'installer. Ce choix est dû au fait qu'il s'agit d'un langage/framework utilisé à PROBTP, étant ainsi dans le monde Web j'ai voulu tester le framework D3.js évoqué en cours. Je n'ai pas réussi à l'intégrer, j'ai donc tout codé nativement à base d'objet SVG à l'exception de l'algorithme de \emph{Treemap} pour lequel j'ai utilisé le framework : squarify~\footnote{\url{https://www.npmjs.com/package/squarify}}. Une grande partie de la logique est pilotée par l'élément app.component.html~\footnote{\url{https://gitlab.com/rletient/rcp216-visu/blob/master/src/app/app.component.html}}. \\Le projet est disponible sous gitlab à l'adresse suivante : \url{https://gitlab.com/rletient/rcp216-visu}

\end{document}




