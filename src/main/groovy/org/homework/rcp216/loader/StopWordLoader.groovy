package org.homework.rcp216.loader

import com.google.common.collect.Sets
import org.apache.lucene.analysis.fr.FrenchAnalyzer
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.broadcast.Broadcast

/**
 * Charge les stop words français issu du projet Lucene
 */
class StopWordLoader {
    private JavaSparkContext sparkContext

    static StopWordLoader create() {
        new StopWordLoader()
    }

    StopWordLoader sparkContext(JavaSparkContext sparkContext) {
        this.sparkContext = sparkContext
        return this
    }

    Broadcast<Set<String>> load() {
        return sparkContext.broadcast(Sets.newHashSet(FrenchAnalyzer.getDefaultStopSet())) as Broadcast<Set<String>>
    }

}
