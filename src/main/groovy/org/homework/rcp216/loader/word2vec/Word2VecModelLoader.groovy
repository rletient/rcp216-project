package org.homework.rcp216.loader.word2vec

import com.medallia.word2vec.Word2VecModelWithEncodingLoader
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import org.apache.http.client.fluent.Request
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.ml.feature.Word2VecModel
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Classe permettant de charger un modèle Word2Vec depuis un format binaire et de le rendre exploitable via Spark
 */
class Word2VecModelLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(Word2VecModelLoader.class)

    private JavaSparkContext sparkContext
    private File workdir
    private String name
    private String url

    static Word2VecModelLoader create() {
        return new Word2VecModelLoader()
    }

    Word2VecModelLoader workdir(File workdir) {
        this.workdir = workdir
        return this
    }

    Word2VecModelLoader url(String url) {
        this.url = url
        return this
    }

    Word2VecModelLoader sparkContext(JavaSparkContext sparkContext) {
        this.sparkContext = sparkContext
        return this
    }

    /**
     * Permet de télécharger le modèle s'il n'est pas présent localement
     * @param toFile
     */
    private void download(File toFile) {
        LOGGER.info("download from $url to ${toFile.getCanonicalPath()}")
        FileUtils.touch(toFile)
        Request.Get(this.url)
                .execute()
                .saveContent(toFile)
    }

    /**
     * Chargement du modèle binaire
     * @return
     */
    Broadcast<Word2VecModel> load() {
        File file = new File(workdir, DigestUtils.md5Hex(this.url))
        if (!file.exists()) {
            download(file)
        }
        Word2VecModel model = new Word2VecModel(UUID.randomUUID().toString(),
                Word2VecModelWithEncodingLoader.fromBinFile(file))
        return sparkContext.broadcast(model)
    }
}
