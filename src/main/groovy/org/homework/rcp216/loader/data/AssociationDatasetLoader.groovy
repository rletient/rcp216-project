package org.homework.rcp216.loader.data

import com.google.common.collect.Lists
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import org.apache.http.client.fluent.Request
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.SparkSession
import org.homework.rcp216.model.Association
import org.jopendocument.dom.spreadsheet.Sheet
import org.jopendocument.dom.spreadsheet.SpreadSheet
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Classe permettant de charger le fichier OpenOffice des associations d'utilité publique sous la forme d'un Dataset
 */
class AssociationDatasetLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssociationDatasetLoader.class)

    private File file

    private JavaSparkContext sparkContext

    private SparkSession sparkSession

    private AssociationDatasetLoader() {}

    /**
     *
     * @return - l'ensemble des objets d'association déclarés au sein du document
     */
    Dataset<Association> getAssociations() {
        Sheet sheet = SpreadSheet.createFromFile(file).getSheet(0)
        Dataset<Association> da = sparkSession.createDataset(Lists.newArrayList(AssociationIterator.basedOn(sheet)), Encoders.bean(Association.class))
        return da
    }

    static Builder builder() {
        return new Builder()
    }

    static class Builder {
        private File workdir
        private String name
        private String url
        private boolean forceRefrestIfExist = false
        private JavaSparkContext sparkContext
        private SparkSession sparkSession

        Builder sparkSession(SparkSession sparkSession) {
            this.sparkSession = sparkSession
            return this
        }

        Builder sparkContext(JavaSparkContext sparkContext) {
            this.sparkContext = sparkContext
            return this
        }

        Builder workdir(File workdir) {
            this.workdir = workdir
            return this
        }

        Builder url(String url) {
            this.url = url
            return this
        }

        Builder forceRefrestIfExist(boolean forceRefrestIfExist) {
            this.forceRefrestIfExist = forceRefrestIfExist
            return this
        }

        /**
         * Télécharge le dataset si nécessaire au sein du répertoire de travail
         */
        private void download(File toFile) {
            LOGGER.info("download from $url to ${toFile.getCanonicalPath()}")
            FileUtils.touch(toFile)
            Request.Get(this.url)
                    .execute()
                    .saveContent(toFile)
        }

        /**
         * Chargement du fichier OpenOffice sous la forme d'un Dataset d'Association
         * @return
         */
        Dataset<Association> load() {
            File targetFile = new File(workdir, DigestUtils.md5Hex(this.url))
            if (targetFile.exists() && forceRefrestIfExist) {
                LOGGER.info("delete old file ${targetFile.getCanonicalPath()}")
                FileUtils.forceDelete(targetFile)
            }

            if (!targetFile.exists()) {
                download(targetFile)
            }

            AssociationDatasetLoader dataset = new AssociationDatasetLoader()
            dataset.file = targetFile
            dataset.sparkContext = this.sparkContext
            dataset.sparkSession = this.sparkSession
            return dataset.getAssociations()
        }
    }
}
