package org.homework.rcp216.loader.data

import org.apache.commons.lang3.StringUtils
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.homework.rcp216.lucene.CustomFrenchAnalyzer
import org.homework.rcp216.model.Association
import org.jopendocument.dom.spreadsheet.Sheet
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class AssociationIterator implements Iterator<Association>, Serializable {

    private Sheet sheet
    private static final int INDEX_COLUMN_CATEGORIE = 7
    private static final int INDEX_COLUMN_OBJET = 4
    private static final int INDEX_COLUMN_NAME = 2

    private int rowIndex = 1


    private Association currentAssociation

    private AssociationIterator() {}

    private CustomFrenchAnalyzer frenchAnalyzer = new CustomFrenchAnalyzer()

    private static final Logger LOGGER = LoggerFactory.getLogger(AssociationIterator.class)

    @Override
    boolean hasNext() {
        String categorie = sheet.getCellAt(INDEX_COLUMN_CATEGORIE, rowIndex).getTextValue()

        TokenStream tokenStream = frenchAnalyzer.tokenStream("categorie", categorie)
        CharTermAttribute attribute = tokenStream.addAttribute(CharTermAttribute.class)
        StringBuffer stringBuffer = new StringBuffer()
        try {
            tokenStream.reset()
            boolean first = true
            while (tokenStream.incrementToken()) {
                if(first) {
                    first = false
                    continue
                }
                stringBuffer.append(attribute.toString())
                stringBuffer.append(" ")
            }
        } catch (IOException e) {
            LOGGER.error("Une erreur est survenue lors du traitement de la phrase : ${association} !", e)
        }
        tokenStream.close()

        categorie = stringBuffer.toString()
        if(categorie.equals("amicales groupts affinitaires ")) {
            categorie = "amicales groupts affinit "
        } else if(categorie.equals("armée anciens comb ")) {
            categorie = "armée anc combattants "
        }
        categorie = StringUtils.stripAccents(categorie)
        String objet = sheet.getCellAt(INDEX_COLUMN_OBJET, rowIndex).getTextValue()
        String name = sheet.getCellAt(INDEX_COLUMN_NAME, rowIndex).getTextValue()
        currentAssociation = new Association(rowIndex, categorie, objet, name)
        return currentAssociation.isNotEmpty()
    }

    @Override
    Association next() {
        rowIndex ++
        return currentAssociation
    }

    static AssociationIterator basedOn(Sheet sheet) {
        AssociationIterator iterator = new AssociationIterator()
        iterator.sheet = sheet
        return iterator
    }
}
