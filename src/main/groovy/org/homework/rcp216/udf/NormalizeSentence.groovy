package org.homework.rcp216.udf

import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.api.java.UDF1
import org.homework.rcp216.lucene.CustomFrenchAnalyzer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Permet de normaliser une phrase en utiliser l'API du projet Lucene.
 */
class NormalizeSentence implements UDF1<String,String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NormalizeSentence.class)

    private Analyzer analyzer

    private NormalizeSentence() {}

    @Override
    String call(String sentence) throws Exception {
        StringBuilder builder = new StringBuilder()
        TokenStream tokenStream = analyzer.tokenStream("sentence", sentence)
        CharTermAttribute attribute = tokenStream.addAttribute(CharTermAttribute.class)
        try {
            tokenStream.reset()
            while (tokenStream.incrementToken()) {
                String term = attribute.toString()
                builder.append(term + " ")
            }
        } catch (IOException e) {
            LOGGER.error("Une erreur est survenue lors du traitement de la phrase : ${sentence} !", e)
        }
        tokenStream.close()
        return builder.toString()
    }

    static Builder builder() {
        return new Builder()
    }

    static class Builder {
        private Broadcast<Set<String>> broadcastStopWord

        Builder broadcastStopWord(Broadcast<Set<String>> broadcastStopWord) {
            this.broadcastStopWord = broadcastStopWord
            return this
        }

        NormalizeSentence build() {
            NormalizeSentence compute = new NormalizeSentence()
            compute.analyzer = new CustomFrenchAnalyzer(broadcastStopWord)
            return compute
        }
    }

}
