package org.homework.rcp216.udf

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.MutableAggregationBuffer
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction
import org.apache.spark.sql.types.DataType
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.types.StructType

/**
 * Permet à la un ensemble de data de StringType d'être concaténé
 */
class ConcatStringUDAF extends UserDefinedAggregateFunction{

    static ConcatStringUDAF createConcatStringUDAF() {
        ConcatStringUDAF concatStringUDAF = new ConcatStringUDAF()
        return concatStringUDAF
    }

    @Override
    StructType inputSchema() {
        return new StructType()
                    .add("concat", DataTypes.StringType)
    }

    @Override
    StructType bufferSchema() {
        return new StructType()
                .add("concat", DataTypes.StringType)
    }

    @Override
    DataType dataType() {
        return DataTypes.StringType
    }

    @Override
    boolean deterministic() {
        return true
    }

    @Override
    void initialize(MutableAggregationBuffer buffer) {
        buffer.update(0, "")
    }

    @Override
    void update(MutableAggregationBuffer buffer, Row input) {
        buffer.update(0, buffer.get(0) + " " + input.get(0))
    }

    @Override
    void merge(MutableAggregationBuffer buffer, Row row) {
        buffer.update(0, buffer.get(0) + " " +row.get(0))
    }

    @Override
    Object evaluate(Row rowOfMyByffer) {
        return rowOfMyByffer.get(0)
    }
}
