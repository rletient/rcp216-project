package org.homework.rcp216.udf

import org.apache.spark.ml.linalg.Vector
import org.apache.spark.ml.linalg.VectorUDT
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.MutableAggregationBuffer
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction
import org.apache.spark.sql.types.DataType
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.types.Metadata
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.homework.rcp216.meta.MetaProgrammation

/**
 * Permet de faire une moyenne sur des colonnes de type Vector
 */
class VectorMeanUDAF extends UserDefinedAggregateFunction{

    static {
        MetaProgrammation.load();
    }

    private final static int COLUMN_VECTOR = 0
    private final static int COLUMN_COUNTER = 1

    static int counterFrom(Row row) {
        return row.getInt(COLUMN_COUNTER)
    }

    static Vector vectorFrom(Row row) {
        return (Vector) row.getAs(COLUMN_VECTOR)
    }

    static VectorMeanUDAF createMeanVectorUDAF() {
        VectorMeanUDAF vectorSum = new VectorMeanUDAF()
        return vectorSum
    }

    @Override
    StructType inputSchema() {
        return new StructType()
                    .add("vector", new VectorUDT())
    }

    @Override
    StructType bufferSchema() {
        return new StructType()
                    .add("vector", new VectorUDT())
                    .add("counter", DataTypes.IntegerType)
    }

    @Override
    DataType dataType() {
        return new VectorUDT()
    }

    @Override
    boolean deterministic() {
        return true
    }

    @Override
    void initialize(MutableAggregationBuffer buffer) {
        // On ne connait pas encore la taille des vectors présents au sein de la colonne
        buffer.update(COLUMN_VECTOR, null)
        buffer.update(COLUMN_COUNTER, 0)
    }

    @Override
    void update(MutableAggregationBuffer buffer, Row input) {
        buffer.update(COLUMN_COUNTER, counterFrom(buffer) + 1)
        buffer.update(COLUMN_VECTOR, input.getAs(0) + vectorFrom(buffer))
    }

    @Override
    void merge(MutableAggregationBuffer buffer, Row row) {
        buffer.update(COLUMN_COUNTER, counterFrom(row) + counterFrom(buffer))
        buffer.update(COLUMN_VECTOR, vectorFrom(row) + vectorFrom(buffer))
    }

    @Override
    Object evaluate(Row rowOfMyByffer) {
        return vectorFrom(rowOfMyByffer) * (1 / counterFrom(rowOfMyByffer))
    }
}
