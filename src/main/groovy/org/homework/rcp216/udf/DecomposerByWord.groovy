package org.homework.rcp216.udf

import com.google.common.collect.Lists
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.apache.spark.api.java.function.PairFlatMapFunction
import org.apache.spark.broadcast.Broadcast
import org.homework.rcp216.lucene.CustomFrenchAnalyzer
import org.homework.rcp216.model.Association
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import scala.Tuple2

/**
 * Classe permettant de décomposer une phrase en liste de mots
 */
class DecomposerByWord implements PairFlatMapFunction<Association, Integer, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DecomposerByWord.class)

    private Analyzer analyzer

    @Override
    Iterator<Tuple2<Integer, String>> call(Association association) throws Exception {
        List<Tuple2<Integer, String>> result = Lists.newArrayList()
        TokenStream tokenStream = analyzer.tokenStream("sentence", association.getObjet())
        CharTermAttribute attribute = tokenStream.addAttribute(CharTermAttribute.class)
        try {
            tokenStream.reset()
            while (tokenStream.incrementToken()) {
                result.add(new Tuple2<Integer, String>(association.getId(), attribute.toString()))
            }
        } catch (IOException e) {
            LOGGER.error("Une erreur est survenue lors du traitement de la phrase : ${association} !", e)
        }
        tokenStream.close()
        return result.iterator()
    }

    static Builder builder() {
        return new Builder()
    }

    static class Builder {
        private Broadcast<Set<String>> broadcastStopWord


        Builder broadcastStopWord(Broadcast<Set<String>> broadcastStopWord) {
            this.broadcastStopWord = broadcastStopWord
            return this
        }

        DecomposerByWord build() {
            DecomposerByWord decompose = new DecomposerByWord()
            decompose.analyzer = new CustomFrenchAnalyzer(broadcastStopWord)
            return decompose
        }
    }
}
