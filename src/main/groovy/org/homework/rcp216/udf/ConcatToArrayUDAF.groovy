package org.homework.rcp216.udf

import com.google.common.collect.Lists
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.MutableAggregationBuffer
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction
import org.apache.spark.sql.types.ArrayType
import org.apache.spark.sql.types.DataType
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructType
import scala.collection.JavaConversions
import scala.collection.mutable.WrappedArray

/**
 * Permet de concaténer un ensemble de data de type StringType sous la forme d'un Array de type String
 */
class ConcatToArrayUDAF extends UserDefinedAggregateFunction {

    static ConcatToArrayUDAF createConcatStringUDAF() {
        ConcatToArrayUDAF concatStringUDAF = new ConcatToArrayUDAF()
        return concatStringUDAF
    }

    @Override
    StructType inputSchema() {
        return new StructType()
                .add("concat", DataTypes.StringType)
    }

    @Override
    StructType bufferSchema() {
        return new StructType()
                .add("concat", new ArrayType(DataTypes.StringType,false))
    }

    @Override
    DataType dataType() {
        return new ArrayType(DataTypes.StringType,false)
    }

    @Override
    boolean deterministic() {
        return true
    }

    @Override
    void initialize(MutableAggregationBuffer buffer) {
        buffer.update(0, new ArrayList<String>())
    }

    @Override
    void update(MutableAggregationBuffer buffer, Row input) {
        String word = input.get(0)
        WrappedArray<String> list = (WrappedArray<String>) buffer.get(0)
        List<String> maList = Lists.newArrayList(JavaConversions.asJavaCollection(list))
        maList.add(word)
        buffer.update(0, maList)
    }

    @Override
    void merge(MutableAggregationBuffer buffer, Row row) {
        List<String> buffers = Lists.newArrayList(JavaConversions.asJavaCollection((WrappedArray<String>) buffer.get(0)))
        List<String> rows = Lists.newArrayList(JavaConversions.asJavaCollection((WrappedArray<String>) row.get(0)))
        buffers.addAll(rows)
        buffer.update(0, buffers)
    }

    @Override
    Object evaluate(Row rowOfMyByffer) {
        return rowOfMyByffer.get(0)
    }
}
