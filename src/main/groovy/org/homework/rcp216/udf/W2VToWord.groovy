package org.homework.rcp216.udf

import com.google.common.base.Joiner
import com.google.common.collect.Lists
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.ml.feature.Word2VecModel
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql.api.java.UDF1
import scala.Tuple2

/**
 * Permet de convertir un Vector sous la forme d'un ensemble de terme (7 maximum) à l'aide d'un modèle Word2Vec.
 */
class W2VToWord implements UDF1<Vector,String> {

    static final int LEMMA = 0;
    static final int TAG = 1;

    private Broadcast<Word2VecModel> model

    W2VToWord(Broadcast<Word2VecModel> model) {
        this.model = model
    }

    @Override
    String call(Vector vector) throws Exception {
        List<String> memory = Lists.newArrayList()
        model.getValue()
                .findSynonymsArray(vector, 50).each { Tuple2<String, Object> tuple ->
            if(memory.size() > 7) {
                return
            }
            String lemmePos = tuple._1()
            String[] part = lemmePos.split("_")
            // On ne sélectionne que les verbes, les noms et les adjectifs
            switch(part[TAG]) {
                case "v":
                    if("avoir".equals(part[LEMMA]) || "être".equals(part[LEMMA])) {
                        break
                    }
                case "n":
                case "a":
                    if(!memory.contains(part[LEMMA])) {
                        memory.add(part[LEMMA] + "_" + tuple._2())
                    }
            }
        }
        return Joiner.on("#").join(memory)
    }
}
