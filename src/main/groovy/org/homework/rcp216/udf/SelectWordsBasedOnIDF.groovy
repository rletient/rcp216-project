package org.homework.rcp216.udf

import com.google.common.base.Joiner
import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.spark.ml.feature.CountVectorizerModel
import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.sql.api.java.UDF2
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import scala.collection.JavaConversions
import scala.collection.mutable.WrappedArray

class SelectWordsBasedOnIDF implements UDF2<WrappedArray<String>, SparseVector, String> {

    private Logger LOGGER = LoggerFactory.getLogger(SelectWordsBasedOnIDF.class)
    String separator = "#"
    int numberOfWords = 5

    Map<String, Integer> registry = Maps.newHashMap();

    SelectWordsBasedOnIDF(CountVectorizerModel model, int numberOfWords) {
        this.numberOfWords = numberOfWords
        int i = 0
        for (String vocabulary : model.vocabulary()) {
            registry.put(vocabulary, i)
            i++
        }
    }

    @Override
    String call(WrappedArray<String> colWords, SparseVector vector) throws Exception {
        List<Word> words = Lists.newArrayList()
        List<String> collection = Lists.newArrayList(JavaConversions.asJavaCollection(colWords))
        for (int i = 0; i < collection.size(); i++) {
            try {
                String word = collection.get(i)
                words.add(new Word(word: word, tfidf: vector.apply(registry.get(word))))
            } catch (Exception e) {
                LOGGER.error("Une erreur est survenue !", e)
            }
        }
        Collections.sort(words)


        int selected = 0;
        List<String> selection = Lists.newArrayList()
        for(int i = 0; i < words.size() ; i++) {
            Word wordToSelect = words.get(i)
            if(selected == numberOfWords) {
                break
            }

            if(!selection.contains(wordToSelect.word)) {
                selection.add(wordToSelect.word)
                selected ++
            }
        }
        return Joiner.on(separator).join(selection).toString()
    }

    class Word implements Comparable<Word> {
        String word
        double tfidf

        @Override
        int compareTo(Word other) {
            double result = other.tfidf - this.tfidf
            if (result == 0) {
                return 0
            }
            return result < 0 ? -1 : 1;
        }
    }
}
