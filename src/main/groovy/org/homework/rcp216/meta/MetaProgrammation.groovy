package org.homework.rcp216.meta

import org.apache.spark.ml.linalg.DenseVector
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.ml.linalg.Vectors
import org.homework.rcp216.utils.ComputeUtils

/**
 * Spécifique à Groovy.
 *
 * Permet l'ajouter et de surcharger les opérateurs pour la classe DenseVector. La classe VectorMeanUDAF on est la bénéficiaire.
 */
class MetaProgrammation {

    static {
        DenseVector.metaClass.multiply << { int value ->
            return delegate.multiply((double) value)
        }

        DenseVector.metaClass.multiply << { float value ->
            return delegate.multiply((double) value)
        }

        DenseVector.metaClass.multiply << { double value ->
            DenseVector myDenseVector = (DenseVector) delegate
            ComputeUtils.multiply(myDenseVector, value)
        }

        DenseVector.metaClass.plus << { Vector vector ->
            DenseVector denseVectorOrigin = (DenseVector) delegate
            if (vector == null) {
                return denseVectorOrigin.copy()
            }
            DenseVector denseVectorToAdd = vector.toDense()
            return ComputeUtils.add(denseVectorOrigin, denseVectorToAdd)
        }

        DenseVector.metaClass.plus << { double[] tab ->
            DenseVector denseVectorOrigin = (DenseVector) delegate
            return denseVectorOrigin + new DenseVector(tab);
        }
    }

    static load() {

    }
}
