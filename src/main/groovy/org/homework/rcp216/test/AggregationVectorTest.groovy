package org.homework.rcp216.test;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.ml.linalg.VectorUDT
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType
import org.homework.rcp216.meta.MetaProgrammation;
import org.homework.rcp216.udf.VectorMeanUDAF;

import static org.apache.spark.sql.functions.*;

class AggregationVectorTest {

    static void main(String[] args) {
        MetaProgrammation.load()

        SparkConf conf = new SparkConf().setMaster("local").setAppName("MyProject")
        JavaSparkContext sc = new JavaSparkContext(conf)
        SparkSession session = SparkSession.builder().config(conf).getOrCreate()
        session.udf().register("meanVector", VectorMeanUDAF.createMeanVectorUDAF())

        Dataset<Row> dataset = session.createDataFrame(
                sc.parallelize(Lists.newArrayList(ForVector.build("1", "[0,0,5]"),
                        ForVector.build("1", "[4,0,1]"),
                        ForVector.build("1", "[1,2,1]"),
                        ForVector.build("2", "[7,5,0]"),
                        ForVector.build("2", "[3,3,4]"),
                        ForVector.build("3", "[0,8,1]"),
                        ForVector.build("3", "[0,0,1]"),
                        ForVector.build("3", "[7,7,7]")
                )).map(new Function<ForVector, Row>() {
                    Row call(ForVector forVector) throws Exception {
                        Vector v = new DenseVector(org.apache.spark.mllib.linalg.Vectors.parse(forVector.stringVector).toArray());
                        return RowFactory.create(forVector.id, v);
                    }
                }), new StructType().add("id", DataTypes.StringType).add("vectorFrom", new VectorUDT()));

        dataset.show()
        dataset.printSchema()
        dataset.groupBy(col("id")).agg(expr("meanVector(vectorFrom) as toto")).show()
    }

    static class ForVector implements Serializable {
        String id;
        String stringVector;

        static ForVector build(String id, String vector) {
            ForVector forVector = new ForVector()
            forVector.id = id
            forVector.stringVector = vector
            return forVector
        }
    }
}
