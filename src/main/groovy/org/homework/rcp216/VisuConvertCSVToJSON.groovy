package org.homework.rcp216

import groovy.json.JsonOutput

class VisuConvertCSVToJSON {

    static final int ID = 0
    static final int CATEGORIE = 1
    static final int IDCLUSTER = 2
    static final int CLUSTERW2VEC = 3

    // ,{"value":1,"partitions":[],"words":[{"word":"","score":1.0}],"name":" "},{"value":1,"partitions":[],"words":[{"word":" ","score":1.0}],"name":" "}

    static void main(String[] args) {
        Random random = new Random()
        File csv = new File("/home/developper/dev/rcp216-workdir/Part6AssociationClassification.csv/part-00000-06c019d3-305d-4fb9-b99b-50c51891214f-c000.csv")
        boolean ignoreLine = true
        Map<String, List<Comptage>> registry = [:]
        csv.eachLine { line ->
            if (ignoreLine) {
                ignoreLine = false
                return
            }
            String[] tokens = line.split("\\|")

            List<Comptage> comptages = registry.get(tokens[IDCLUSTER] +"~"+tokens[CLUSTERW2VEC], [])
            Comptage comptage = comptages.find { it.name == tokens[CATEGORIE] }
            if (comptage == null) {
                comptage = new Comptage([name: tokens[CATEGORIE], value: 0])
                comptages << comptage
            }
            comptage.value++

        }

        List<Cluster> clusters = []

        registry.each { key, value ->

            List<Word> words = []
            String clusterId = key.split("~")[0]
            String[] pairs = key.split("~")[1].split("#")
            pairs.each { pair ->
                String[] tokens = pair.split("_")
                words << new Word([word: tokens[0], score: Float.parseFloat(tokens[1].subSequence(0, 4))])
            }

            int size = 0
            List<Partition> partitions = []
            value.each { comptage ->
                partitions << new Partition([name: comptage.name, value: comptage.value])
                size += comptage.value
            }

            clusters << new Cluster([name: "$clusterId", value: size, words: words.subList(0, 4), partitions: partitions])
        }
        def json = JsonOutput.toJson(clusters)
        println json
    }

    static class Cluster {
        String name;
        int value
        List<Word> words = []
        List<Partition> partitions = []
    }

    static class Word {
        String word
        float score
    }

    static class Partition {
        String name
        float value
    }

    static class Comptage {
        String name
        int value
    }

}
