package org.homework.rcp216.opennlp

import groovy.json.JsonSlurper
import opennlp.tools.lemmatizer.Lemmatizer

/**
 * Permet de fournir notre propre dictionnaire de lemme. Je n'ai pas réussi à utiliser celui fournit nativement avec OpenNLP, j'ai donc créé le mien à partir
 * de http://www.cnrtl.fr/corpus/perceo/
 */
class PersonnalLemmeDictionnary implements Lemmatizer, Serializable {

    private Map<String, List<PostagToLemma>> repository

    private static class PostagToLemma implements Serializable {
        String postag
        String lemma

        boolean postTagEquals(String postagOpenNLP) {
            switch (postagOpenNLP) {
                case 'ADJ':
                case 'A':
                case 'ADJWH':
                    return "ADJ".equals(postag) || "ADJ:trc".equals(postag)
                case 'ADV':
                case 'Adv':
                case 'ADVWH':
                    return "ADV".equals(postag)
                case 'CC':
                case 'C':
                case 'CS':
                    return "KON".equals(postag)

                case 'DET':
                case 'DETWH':
                    return "DET:def".equals(postag) ||
                            "DET:dem".equals(postag) ||
                            "DET:ind".equals(postag) ||
                            "DET:int".equals(postag) ||
                            "DET:par".equals(postag) ||
                            "DET:pos".equals(postag) ||
                            "DET:pre".equals(postag)
                case 'NC':
                case 'N':
                case 'NPP':
                    return "NAM".equals(postag) ||
                            "NAM:sig".equals(postag) ||
                            "NAM:trc".equals(postag) ||
                            "NOM".equals(postag) ||
                            "NOM:sig".equals(postag) ||
                            "NOM:trc".equals(postag)
                case 'CLO':
                case 'CLR':
                case 'CLS':
                case 'PRO':
                case 'PROREL':
                case 'PROWH':
                    return "PRO:clo".equals(postag) ||
                            "PRO:cls".equals(postag) ||
                            "PRO:clsi".equals(postag) ||
                            "PRO:dem".equals(postag) ||
                            "PRO:ind".equals(postag) ||
                            "PRO:int".equals(postag) ||
                            "PRO:pos".equals(postag) ||
                            "PRO:rel".equals(postag) ||
                            "PRO:ton".equals(postag)
                case 'V':
                case 'VIMP':
                case 'VINF':
                case 'VPP':
                case 'VPR':
                case 'VS':
                    return "AUX:cond".equals(postag) ||
                            "AUX:futu".equals(postag) ||
                            "AUX:impe".equals(postag) ||
                            "AUX:impf".equals(postag) ||
                            "AUX:infi".equals(postag) ||
                            "AUX:pper".equals(postag) ||
                            "AUX:ppre".equals(postag) ||
                            "AUX:pres".equals(postag) ||
                            "AUX:simp".equals(postag) ||
                            "AUX:subi".equals(postag) ||
                            "AUX:subp".equals(postag) ||
                            "VER".equals(postag) ||
                            "VER:cond".equals(postag) ||
                            "VER:futu".equals(postag) ||
                            "VER:impe".equals(postag) ||
                            "VER:impf".equals(postag) ||
                            "VER:infi".equals(postag) ||
                            "VER:pper".equals(postag) ||
                            "VER:ppre".equals(postag) ||
                            "VER:pres".equals(postag) ||
                            "VER:simp".equals(postag) ||
                            "VER:subi".equals(postag) ||
                            "VER:subp".equals(postag) ||
                            "VER:trc".equals(postag)
                case 'Num':
                    return "NUM".equals(postag)
            }
            return true
        }
    }

    @Override
    String[] lemmatize(String[] tokens, String[] postags) {
        if (tokens.size() != postags.size()) {
            throw new IllegalArgumentException("La taille des 2 paramètres doit être similaire")
        }
        String[] lemmas = new String[postags.size()]
        tokens.size().times { cpt ->
            lemmas[cpt] = lemmatize(tokens[cpt], postags[cpt])
        }
        return lemmas
    }

    String lemmatize(String token, String postag) {
        List<PostagToLemma> postagToLemmas = repository.get(token)
        if (!postagToLemmas) {
            return token
        }

        String lemma = postagToLemmas.find { it.postTagEquals(postag) }?.lemma
        if (lemma) {
            return lemma
        }

        return postagToLemmas[0].lemma
    }


    @Override
    List<List<String>> lemmatize(List<String> tokens, List<String> postags) {
        throw new UnsupportedOperationException()
    }


    private PersonnalLemmeDictionnary() {}

    static PersonnalLemmeDictionnary load(File file) {
        Map<String, List<PostagToLemma>> repository = [:]
        def jsonSlurper = new JsonSlurper()
        Set<String> repoForm = []
        // Un bonne exemple le mot vert > grep "\"vert\"" work.json
        file.eachLine { line ->
            def object = jsonSlurper.parseText(line)
            List<PostagToLemma> postagToLemmas = []
            object.each { word, forms ->
                forms['gr'].each { form, lemme ->
                    postagToLemmas << new PostagToLemma(postag: form, lemma: lemme)
                }
                repository[word] = postagToLemmas
            }
        }
        PersonnalLemmeDictionnary lemmeDictionnary = new PersonnalLemmeDictionnary()
        lemmeDictionnary.repository = repository
        return lemmeDictionnary
    }
}
