package org.homework.rcp216

import groovy.json.JsonOutput

class VisuCategorieCSVToJSON {

    static final int CATEGORIE = 0
    static final int WORDS = 1

    static void main(String[] args) {
        //File csv = new File("/home/developper/dev/rcp216-workdir/Part8CategorieTFIdf.csv/part-00000-1e7b7002-c147-401e-9227-dd8398409c7e-c000.csv")
        File csv = new File("/home/developper/dev/rcp216-workdir/Part7CategorieToW2V.csv/part-00000-2789dbfc-c802-46bc-ae8f-3693009f8c18-c000.csv")
        boolean ignoreLine = true

        List<Cluster> clusters = []

        csv.eachLine { line ->
            if (ignoreLine) {
                ignoreLine = false
                return
            }
            String[] tokens = line.split("\\|")
            List<Word> words = []
            tokens[WORDS].split("#").each { element ->
                if (element.contains("_")) {
                    String[] pairs = element.split("_")
                    words << new Word(word: pairs[0], score: Float.parseFloat(pairs[1]))
                } else {
                    words << new Word(word: element, score: 1d)
                }
            }
            clusters << new Cluster([name: tokens[CATEGORIE], value: 1, words: words])

        }
        Collections.sort(clusters)
        def json = JsonOutput.toJson(clusters)
        println json
    }

    static class Cluster implements Comparable<Cluster> {
        String name;
        int value
        List<Word> words = []
        List<Partition> partitions = []

        int compareTo(Cluster other) {
            return this.name.compareTo(other.name)
        }

    }

    static class Word {
        String word
        float score
    }

    static class Partition {
        String name
        float value
    }

    static class Comptage {
        String name
        int value
    }

}
