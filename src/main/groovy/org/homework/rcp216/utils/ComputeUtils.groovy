package org.homework.rcp216.utils

import org.apache.spark.ml.linalg.DenseVector

class ComputeUtils {

    /**
     * Permet de faire la somme de 2 vecteurs
     * @param denseVectorOrigin
     * @param denseVectorToAdd
     * @return
     */
    static DenseVector add(DenseVector denseVectorOrigin, DenseVector denseVectorToAdd) {
        if(denseVectorToAdd == null) {
            return denseVectorOrigin.copy()
        }

        if(denseVectorOrigin.size() != denseVectorToAdd.size()) {
            throw new UnsupportedOperationException("Les 2 vecteurs n'ont pas la même taille")
        }
        double[] result = new double[denseVectorOrigin.size()]
        for(int i = 0; i < denseVectorOrigin.size(); i++) {
            result[i] = denseVectorOrigin.apply(i) + denseVectorToAdd.apply(i)
        }
        return new DenseVector(result)
    }

    /**
     * Permet de multiplier un vecteur par un double
     * @param vector
     * @param value
     * @return
     */
    static DenseVector multiply(DenseVector vector, double value) {
        double[] result = new double[vector.size()]
        for(int i = 0; i < vector.size(); i++) {
            result[i] = vector.apply(i) * value
        }
        return new DenseVector(result)
    }
}
