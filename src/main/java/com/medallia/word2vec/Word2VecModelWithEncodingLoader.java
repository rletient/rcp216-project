package com.medallia.word2vec;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.medallia.word2vec.util.AC;
import com.medallia.word2vec.util.ProfilingTimer;
import org.apache.spark.mllib.feature.Word2VecModel;
import org.homework.rcp216.spark.StopWordsUtils;
import scala.Predef;
import scala.Tuple2;
import scala.collection.JavaConverters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.*;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * Classe très largement inspiré du projet medallia pour le chargement de modèle word2vec au format binaire
 *
 * @see <a href="https://github.com/medallia/Word2VecJava">Medallia</a>
 */
public class Word2VecModelWithEncodingLoader {

    private static final long ONE_GB = 1073741824L;

    private Word2VecModelWithEncodingLoader() {
    }


    public static Word2VecModel fromBinFile(File file) throws IOException {
        return fromBinFile(file, ByteOrder.LITTLE_ENDIAN, ProfilingTimer.NONE);
    }

    public static Word2VecModel fromBinFile(File file, ByteOrder byteOrder) throws IOException {
        return fromBinFile(file, byteOrder, ProfilingTimer.NONE);
    }

    public static Word2VecModel fromBinFile(File file, ByteOrder byteOrder, ProfilingTimer timer) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        Set<String> stopWords = StopWordsUtils.set();
        Throwable var4 = null;
        try {
            AC ac = timer.start("Loading vectors from bin file", new Object[0]);
            Throwable var6 = null;
            Map<String, float[]> vectorsToReturn = Maps.newHashMap();
            try {
                FileChannel channel = fis.getChannel();
                timer.start("Reading gigabyte #1", new Object[0]);
                MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0L, Math.min(channel.size(), 2147483647L));
                buffer.order(byteOrder);
                int bufferCount = 1;
                StringBuilder sb = new StringBuilder();
                char c;
                for (c = (char) buffer.get(); c != '\n'; c = (char) buffer.get()) {
                    sb.append(c);
                }
                String firstLine = sb.toString();
                int index = firstLine.indexOf(32);
                Preconditions.checkState(index != -1, "Expected a space in the first line of file '%s': '%s'", new Object[]{file.getAbsolutePath(), firstLine});
                int vocabSize = Integer.parseInt(firstLine.substring(0, index));
                int layerSize = Integer.parseInt(firstLine.substring(index + 1));
                int cpt = 0;
                timer.appendToLog(String.format("Loading %d vectors with dimensionality %d", vocabSize, layerSize));
                List<String> vocabs = new ArrayList(vocabSize);
                DoubleBuffer vectorsDoubleBuffer = ByteBuffer.allocateDirect(vocabSize * layerSize * 8).asDoubleBuffer();
                long lastLogMessage = System.currentTimeMillis();

                for (int lineno = 0; lineno < vocabSize; ++lineno) {
                    sb.setLength(0);

                    for (c = (char) buffer.get(); c != ' '; c = (char) buffer.get()) {
                        if (c != '\n' && c != 65475) {
                            switch (c) {
                                case 65450:
                                    c = 'ê';
                                    break;
                                case 65449:
                                    c = 'é';
                                    break;
                                case 65442:
                                    c = 'â';
                                    break;
                                case 65448:
                                    c = 'è';
                                    break;
                                case 65465:
                                    c = 'ù';
                                    break;
                                case 65467:
                                    c = 'û';
                                    break;
                                case 65447:
                                    c = 'ç';
                                    break;
                                case 65440:
                                    c = 'à';
                                    break;
                                case 65455:
                                    c = 'ï';
                                    break;
                                case 65460:
                                    c = 'ô';
                                    break;
                                case 65454:
                                    c = 'î';
                                    break;
                                case 65429:
                                    c = '\'';
                                    break;
                                default:
                            }
                            sb.append(c);
                        }
                    }
                    String word = sb.toString();
                    vocabs.add(word);
                    FloatBuffer floatBuffer = buffer.asFloatBuffer();
                    float[] floats = new float[layerSize];
                    floatBuffer.get(floats);
                    if (!stopWords.contains(word)) {
                        vectorsToReturn.put(word, floats);
                    }
                    for (int i = 0; i < floats.length; ++i) {
                        vectorsDoubleBuffer.put(lineno * layerSize + i, (double) floats[i]);
                    }

                    buffer.position(buffer.position() + 4 * layerSize);
                    long now = System.currentTimeMillis();
                    if (now - lastLogMessage > 1000L) {
                        double percentage = (double) (lineno + 1) / (double) vocabSize * 100.0D;
                        timer.appendToLog(String.format("Loaded %d/%d vectors (%f%%)", lineno + 1, vocabSize, percentage));
                        lastLogMessage = now;
                    }

                    if ((long) buffer.position() > 1073741824L) {
                        int newPosition = (int) ((long) buffer.position() - 1073741824L);
                        long size = Math.min(channel.size() - 1073741824L * (long) bufferCount, 2147483647L);
                        timer.endAndStart("Reading gigabyte #%d. Start: %d, size: %d", new Object[]{bufferCount, 1073741824L * (long) bufferCount, size});
                        buffer = channel.map(FileChannel.MapMode.READ_ONLY, 1073741824L * (long) bufferCount, size);
                        buffer.order(byteOrder);
                        buffer.position(newPosition);
                        ++bufferCount;
                    }
                }
                timer.end();
                return new Word2VecModel(toScalaMap(Maps.newHashMap(vectorsToReturn)));
            } catch (Throwable var49) {
                var6 = var49;
                throw var49;
            } finally {
                if (ac != null) {
                    if (var6 != null) {
                        try {
                            ac.close();
                        } catch (Throwable var48) {
                            var6.addSuppressed(var48);
                        }
                    } else {
                        ac.close();
                    }
                }

            }
        } catch (Throwable var51) {
            var4 = var51;
            throw var51;
        } finally {
            if (fis != null) {
                if (var4 != null) {
                    try {
                        fis.close();
                    } catch (Throwable var47) {
                        var4.addSuppressed(var47);
                    }
                } else {
                    fis.close();
                }
            }

        }
    }

    static <A, B> scala.collection.immutable.Map<A, B> toScalaMap(HashMap<A, B> m) {
        return JavaConverters.mapAsScalaMapConverter(m).asScala().toMap(
                Predef.<Tuple2<A, B>>conforms()
        );
    }
}
