package org.homework.rcp216.opennlp;

import com.google.common.collect.Lists;
import opennlp.tools.lemmatizer.Lemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTagger;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.homework.rcp216.model.Association;
import scala.Tuple2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import static com.google.common.base.Preconditions.*;

/**
 * Classe permettant la décomposition d'une phrase ou d'un suite de phrase séparer par un retour chariot
 */
public class DecomposerByLemmaPos implements PairFlatMapFunction<Association, Integer, String> {

    private transient Tokenizer tokenizer;
    private transient POSTagger posTagger;

    private Lemmatizer lemmatizer;

    private boolean enablePostaggingReport = true;

    private TokenizerModel tokenizerModel;
    private POSModel posModel;

    private void initOpenNLPObject() {
        posTagger = new POSTaggerME(posModel);
        tokenizer = new TokenizerME(tokenizerModel);
    }

    private void writeObject(java.io.ObjectOutputStream stream)
            throws java.io.IOException {
        stream.defaultWriteObject();
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws java.io.IOException, ClassNotFoundException {
        stream.defaultReadObject();
        this.initOpenNLPObject();
    }

    /**
     * Décompose chacune des associations en liste de lemme étiqueté grammaticalement.
     * @param association
     * @return
     * @throws Exception
     */
    public Iterator<Tuple2<Integer, String>> call(final Association association) throws Exception {
        List<Tuple2<Integer, String>> tuples = Lists.newArrayList();
        List<String> response = digest(association.getObjet());
        response.forEach(word -> tuples.add(new Tuple2<>(association.getId(), word)));
        return tuples.iterator();
    }

    /**
     * Décomposition d'une chaine de caractere en lemme + position grammaticale
     * @param paragrah
     * @return
     */
    public List<String> digest(String paragrah) {
        String[] sentences = paragrah.split("\n");

        List<String> response = Lists.newArrayList();
        for (String sentence : sentences) {
            StringBuffer filteredSentence = new StringBuffer();
            for (char c : sentence.toCharArray()) {
                // Nettoyage de la chaine de caractères
                switch (c) {
                    case '\'':
                    case '/':
                    case ';':
                    case ':':
                    case '«':
                    case '»':
                    case '(':
                    case ')':
                    case '.':
                    case '"':
                    case '=':
                    case '|':
                    case '+':
                    case '•':
                    case '‘':
                    case '’':
                    case ',':
                        filteredSentence.append(' ');
                        continue;
                    case 'œ':
                        filteredSentence.append('o');
                        filteredSentence.append('e');
                        continue;
                }
                filteredSentence.append(c);
            }
            String[] tokens = tokenizer.tokenize(filteredSentence.toString());
            String[] tags = posTagger.tag(tokens);
            String[] lemmas = lemmatizer.lemmatize(tokens, tags);
            for (int i = 0; i < lemmas.length; i++) {

                if (lemmas[i].equals("l") || lemmas[i].equals("s")) {
                    continue;
                }
                String tag = null;
                switch (tags[i]) {
                    case "ADJ":
                    case "A":
                    case "ADJWH":
                        tag = "a";
                        break;
                    case "ADV":
                    case "Adv":
                    case "ADVWH":
                        tag = "adv";
                        break;
                    case "NC":
                    case "NPP":
                    case "N":
                        tag = "n";
                        break;
                    case "V":
                    case "VIMP":
                    case "VINF":
                    case "VPP":
                    case "VPR":
                    case "VS":
                        tag = "v";
                        break;

                }
                if (tag != null) {
                    if (this.enablePostaggingReport) {
                        response.add(lemmas[i].toLowerCase() + '_' + tag);
                    } else {
                        response.add(lemmas[i].toLowerCase());
                    }
                }
            }
        }
        return response;
    }

    private DecomposerByLemmaPos() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private File lemmaDictionnaryFile;
        private File tokenModelFile;
        private File posModelFile;
        private boolean enablePostaggingReport = true;

        public Builder enablePostaggingReport(boolean enablePostaggingReport) {
            this.enablePostaggingReport = enablePostaggingReport;
            return this;
        }

        public Builder lemmaDictionnaryLocation(File file) {
            this.lemmaDictionnaryFile = file;
            return this;
        }

        public Builder tokenModelLocation(File file) {
            this.tokenModelFile = file;
            return this;
        }

        public Builder posModelLocation(File file) {
            this.posModelFile = file;
            return this;
        }

        public DecomposerByLemmaPos build() throws IOException {
            checkNotNull(lemmaDictionnaryFile);
            checkNotNull(posModelFile);
            checkNotNull(tokenModelFile);
            checkArgument(lemmaDictionnaryFile.exists());
            checkArgument(posModelFile.exists());
            checkArgument(tokenModelFile.exists());

            TokenizerModel tokenizerModel = new TokenizerModel(tokenModelFile);
            POSModel posModel = new POSModel(posModelFile);
            PersonnalLemmeDictionnary lemmeDictionnary = PersonnalLemmeDictionnary.load(lemmaDictionnaryFile);

            DecomposerByLemmaPos decomposerByLemmaPos = new DecomposerByLemmaPos();
            decomposerByLemmaPos.lemmatizer = lemmeDictionnary;
            decomposerByLemmaPos.posModel = posModel;
            decomposerByLemmaPos.tokenizerModel = tokenizerModel;
            decomposerByLemmaPos.enablePostaggingReport = this.enablePostaggingReport;

            decomposerByLemmaPos.initOpenNLPObject();

            return decomposerByLemmaPos;
        }

    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));
            DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                    .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                    .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                    .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                    .build();
            //decomposerByLemmaPos.digest("- permettre à chacun d'être acteur /actrice de la construction.").forEach(System.out::println);
            decomposerByLemmaPos.digest("Aide à l'enfance en détresse. Force de proposition dans le cadre de l'application de la Convention des droits de l'enfant. Aide et partenariat avec les associations membres et les autres.").forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
