package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.feature.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.homework.rcp216.udf.SelectWordsBasedOnIDF;

import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;

public class Test {

    public static void main(String[] args) {

        SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
        conf.set("executor-memory", "7g");
        conf.set("spark.sql.shuffle.partitions", "4");
        JavaSparkContext sparkContext = new JavaSparkContext(conf);
        SparkSession spark = SparkSession.builder().config(conf).getOrCreate();


        // documents corpus. each row is a document.
        List<Row> data = Arrays.asList(
                RowFactory.create(0.0, "Welcome to TutorialKart"),
                RowFactory.create(0.0, "TutorialKart Welcome"),
                RowFactory.create(1.0, "Welcome Remy Remy")
        );
        StructType schema = new StructType(new StructField[]{
                new StructField("label", DataTypes.DoubleType, false, Metadata.empty()),
                new StructField("sentence", DataTypes.StringType, false, Metadata.empty())
        });

        // import data with the schema
        Dataset<Row> sentenceData = spark.createDataFrame(data, schema);

        // break sentence to words
        Tokenizer tokenizer = new Tokenizer().setInputCol("sentence").setOutputCol("words");
        Dataset<Row> wordsData = tokenizer.transform(sentenceData);

        // define Transformer, HashingTF
        CountVectorizer countVectorizer = new CountVectorizer().setInputCol("words").setOutputCol("rawFeatures");


        CountVectorizerModel countModel = countVectorizer.fit(wordsData);
        Dataset<Row> featurizedData = countModel.transform(wordsData);

        /*HashingTF hashingTF = new HashingTF()
                .setInputCol("words")
                .setOutputCol("rawFeatures");

        Dataset<Row> featurizedData = hashingTF.transform(wordsData);
*/

        System.out.println("TF vectorized data\n----------------------------------------");
        for (Row row : featurizedData.collectAsList()) {
            System.out.println(row.get(3));
        }

        featurizedData.printSchema();
        System.out.println(featurizedData.toJSON());

        // IDF is an Estimator which is fit on a dataset and produces an IDFModel
        IDF idf = new IDF().setInputCol("rawFeatures").setOutputCol("features");
        IDFModel idfModel = idf.fit(featurizedData);

        // The IDFModel takes feature vectors (generally created from HashingTF or CountVectorizer) and scales each column
        Dataset<Row> rescaledData = idfModel.transform(featurizedData);
        for (Row row : rescaledData.collectAsList()) {
            System.out.println(row);
        }


        spark.udf().register("selectOnIDF", new SelectWordsBasedOnIDF(countModel, 5), DataTypes.StringType);

        rescaledData
                .withColumn("selection", callUDF("selectOnIDF", col("words"), col("features"))).show();
        spark.close();

    }


}

