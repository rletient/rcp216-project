package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.feature.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.udf.ConcatStringUDAF;
import org.homework.rcp216.udf.ConcatToArrayUDAF;
import org.homework.rcp216.udf.SelectWordsBasedOnIDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.expr;

public class Part8CategorieTFIdf {
    private static final Logger LOGGER = LoggerFactory.getLogger(Part8CategorieTFIdf.class);

    private static final String NAME = "Part8CategorieTFIdf";

    public static File getPathInWorkdir(Properties properties, String name) throws IOException {
        return new File(properties.getProperty("workdir"), name);
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            File fDataset = getPathInWorkdir(properties, NAME);

            // Chargement du fichier des associations sous la forme d'un Dataset
            Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("document.url"))
                    .forceRefrestIfExist(false)
                    .sparkContext(sparkContext)
                    .sparkSession(session).load();

            DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                    .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                    .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                    .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                    .enablePostaggingReport(false)
                    .build();

            session.udf().register("concat", ConcatToArrayUDAF.createConcatStringUDAF());
            Dataset<Row> idMotDataset = Association.splitAssocationByLemmaPos(sparkContext, session, associationDataset, decomposerByLemmaPos);


            idMotDataset = idMotDataset.withColumnRenamed("id", "idAssocMot");

            Dataset<Row> words = associationDataset.join(idMotDataset, col("idAssocMot").equalTo(col("id")))
                    .select(col("categorie"), col("mot")).repartition(1);

            // id | mot
            words = words
                    .groupBy(col("categorie"))
                    .agg(expr(("concat(mot) as words")));

            CountVectorizer countVectorizer = new CountVectorizer().setInputCol("words");
            CountVectorizerModel countModel = countVectorizer.fit(words).setInputCol("words")
                    .setOutputCol("TF");
            Dataset<Row> wordsCounted = countModel.transform(words);

            IDF idf = new IDF().setInputCol("TF").setOutputCol("idf");
            IDFModel idfModel = idf.fit(wordsCounted);
            Dataset<Row> wordsIDFed = idfModel.transform(wordsCounted);

            session.udf().register("selectOnIDF", new SelectWordsBasedOnIDF(countModel,8), DataTypes.StringType);
            wordsIDFed.withColumn("selection", callUDF("selectOnIDF", col("words"), col("idf")))
                    .select(col("categorie"), col("selection"))
                    .repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter", "|")
                    .mode(SaveMode.Overwrite)
                    .save(getPathInWorkdir(properties, NAME + ".csv").getCanonicalPath());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

