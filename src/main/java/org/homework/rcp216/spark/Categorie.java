package org.homework.rcp216.spark;

import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.Row;

import java.io.Serializable;

public class Categorie implements VoidFunction<Row>, Serializable {
    private Broadcast<Word2VecModel> model;


    private Categorie(Broadcast<Word2VecModel> model) {
        this.model = model;
    }

    @Override
    public void call(Row row) throws Exception {
        System.out.println((String) row.getAs("categorie"));
        model.getValue().findSynonyms((Vector) row.getAs("meanVector"), 10).show();
    }

    public static  Categorie displaySynonymsBasedOn(Broadcast<Word2VecModel> model) {
        return new Categorie(model);
    }

}
