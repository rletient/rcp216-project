package org.homework.rcp216.spark;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class StopWordsUtils {

    public static Dataset<Row> dataset(SparkSession session) {
        List<Row> rows = Lists.newArrayList();
        for (Iterator<Object> it = FrenchAnalyzer.getDefaultStopSet().iterator(); it.hasNext(); ) {
            char[] t = (char[]) it.next();
            rows.add(RowFactory.create(new String(t)));
        }
        return session.createDataFrame(rows, new StructType().add("stopWord", DataTypes.StringType));
    }

    public static Set<String> getStopWord() {
        Set<String> set = Sets.newHashSet();
        for (Iterator<Object> it = FrenchAnalyzer.getDefaultStopSet().iterator(); it.hasNext(); ) {
            char[] t = (char[]) it.next();
            set.add(new String(t));
        }
        return set;
    }

    public static Set<String> set() {
        Set<String> set = Sets.newHashSet(getStopWord());
        set.add("l'");
        set.add("s'");
        set.add("d'");
        set.add("où");
        set.add("aussi");
        set.add("dont");
        set.add("également");
        set.add("ainsi");
        set.add("tous");
        set.add("tout");
        set.add("toute");
        set.add("toutes");
        set.add("notamment");
        set.add("plus");
        set.add("ainsi");
        return set;
    }
}
