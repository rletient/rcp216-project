package org.homework.rcp216.spark;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

import static scala.collection.JavaConversions.asJavaIterator;

/**
 * Classe permettant d'écrire dans un fichier de sortie les données  d'un Vector au format csv
 */
public class VectorToFile {

    private static final Logger LOGGER = LoggerFactory.getLogger(VectorToFile.class);

    private CSVPrinter csvPrinter;

    private String[] headers = null;
    private boolean injectHeader = true;

    private VectorToFile(CSVPrinter csvPrinter) {
        this.csvPrinter = csvPrinter;
    }

    public void print(Row row) {
        try {
            injectHeader();
            Vector vector = (Vector) row.get(0);
            csvPrinter.printRecord(Arrays.asList(ArrayUtils.toObject(vector.toArray())));
        } catch (Exception e) {
            LOGGER.error("Une erreur est survenue !", e);
        }
    }

    public void close() throws IOException {
        csvPrinter.flush();
        csvPrinter.close();
    }

    private void injectHeader() throws IOException {
        if (!injectHeader) {
            return;
        }
        csvPrinter.printRecord(headers);
        injectHeader = false;
    }

    public VectorToFile headers(String... headers) {
        this.headers = headers;
        return this;
    }

    public static VectorToFile writeTo(String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);

        return new VectorToFile(csvPrinter);
    }


}
