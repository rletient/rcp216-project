package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.plans.JoinType;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.loader.word2vec.Word2VecModelLoader;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.udf.ConcatStringUDAF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.expr;

public class Part3ChallengeModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(Part3ChallengeModel.class);

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("document.url"))
                    .forceRefrestIfExist(false)
                    .sparkContext(sparkContext)
                    .sparkSession(session).load();


            DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                    .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                    .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                    .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                    .build();

            // id | mot
            Dataset<Row> rows = Association.splitAssocationByLemmaPos(sparkContext, session, associationDataset, decomposerByLemmaPos);

            LOGGER.info("Nombre de mots extraits {}", rows.select(col("mot")).distinct().count());

            // On ne sélectionne que les mots contenues au sein du modèle
            Dataset<Row> modelWordsRows = Word2VecModelLoader.create()
                    .sparkContext(sparkContext)
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("word2vec.model.url"))
                    .load().getValue().getVectors().select(col("word"));

            // On exporte les mots qui ne sont pas contenus dans le modèle
            rows.join(modelWordsRows, col("mot").equalTo(col("word")), "leftouter").repartition(1)
                    .filter(col("word")
                            .isNull()).repartition(1)
                    .select("mot")
                    .distinct()
                    .orderBy(col("mot"))
                    .repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter", "|")
                    .mode(SaveMode.Overwrite)
                    .save(new File(properties.getProperty("workdir"), "challengeModel")
                            .getCanonicalPath());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
