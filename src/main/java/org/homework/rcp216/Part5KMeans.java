package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.evaluation.ClusteringEvaluator;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.loader.word2vec.Word2VecModelLoader;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Part5KMeans {
    private static final Logger LOGGER = LoggerFactory.getLogger(Part5KMeans.class);

    private static final String NAME = "Part5KMeans";

    public static File getPathInWorkdir(Properties properties, String name) throws IOException {
        return new File(properties.getProperty("workdir"), name);
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            File fDataset = getPathInWorkdir(properties,NAME);
            Dataset<Row> datasetIdAssocAndMeanVector = null;
            if(fDataset.exists()) {
                datasetIdAssocAndMeanVector = session.read().parquet(fDataset.getCanonicalPath());
            } else {

                // Chargement du fichier des associations sous la forme d'un Dataset
                Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                        .workdir(new File(properties.getProperty("workdir")))
                        .url(properties.getProperty("document.url"))
                        .forceRefrestIfExist(false)
                        .sparkContext(sparkContext)
                        .sparkSession(session).load();

                // Création de l'instance permettant la décomposition de phrases en lemme + pos
                DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                        .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                        .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                        .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                        .build();

                // Chargement du model Word2Vec
                Broadcast<Word2VecModel> model = Word2VecModelLoader.create()
                        .sparkContext(sparkContext)
                        .workdir(new File(properties.getProperty("workdir")))
                        .url(properties.getProperty("word2vec.model.url"))
                        .load();

                // Création d'un dataset id association | moyenne des vecteurs des mots relatifs à l'association
                datasetIdAssocAndMeanVector = Association.toWord2VecOptimized(sparkContext, session, associationDataset, model, decomposerByLemmaPos).persist(StorageLevel.MEMORY_ONLY_SER());

                // Sauvegarde du modèle
                datasetIdAssocAndMeanVector.write().mode(SaveMode.Overwrite).save(fDataset.getCanonicalPath());

            }

            StringBuffer strBuffer = new StringBuffer();
            for(int k = 2; k < 101 ; k++) {
                KMeansModel kmeansModel = new KMeans().setK(k).setMaxIter(200).setSeed(1L).fit(datasetIdAssocAndMeanVector);
                double wsse = kmeansModel.computeCost(datasetIdAssocAndMeanVector);
                Dataset<Row> predictions = kmeansModel.transform(datasetIdAssocAndMeanVector);
                ClusteringEvaluator evaluator = new ClusteringEvaluator();
                double silhouette = evaluator.evaluate(predictions);
                LOGGER.info(">>> indice silhouette pour K = {} : {} , somme inertie intra groupe : {}", k, silhouette, wsse);
                strBuffer.append(String.format(">>> indice silhouette pour K = %s : %s , somme inertie intra groupe : %s", k, silhouette, wsse)).append("\n");
            }
            LOGGER.info(">>> RESUME : \n {}", strBuffer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}

