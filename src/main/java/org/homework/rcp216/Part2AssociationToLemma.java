package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.udf.ConcatStringUDAF;
import org.homework.rcp216.udf.VectorMeanUDAF;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import static org.apache.spark.sql.functions.*;

public class Part2AssociationToLemma {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("document.url"))
                    .forceRefrestIfExist(false)
                    .sparkContext(sparkContext)
                    .sparkSession(session).load();


            DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                    .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                    .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                    .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                    .enablePostaggingReport(true)
                    .build();

            session.udf().register("concat", ConcatStringUDAF.createConcatStringUDAF());

            // id | mot
            Dataset<Row> rows = Association.splitAssocationByLemmaPos(sparkContext, session, associationDataset, decomposerByLemmaPos)
                    .groupBy(col("id"))
                    .agg(expr(("concat(mot) as mots")))
                    .withColumnRenamed("id", "idSplit");

            associationDataset.join(rows, col("idSplit").equalTo(col("id")))
                    .select(col("objet"), col("mots"))
                    .repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter", "|")
                    .mode(SaveMode.Overwrite)
                    .save(new File(properties.getProperty("workdir"), "lemma")
                            .getCanonicalPath());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
