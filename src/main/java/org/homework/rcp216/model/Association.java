package org.homework.rcp216.model;

import com.google.common.collect.Maps;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.ml.linalg.VectorUDT;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.spark.StopWordsUtils;
import org.homework.rcp216.udf.DecomposerByWord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;

import java.util.Map;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.expr;
import static org.homework.rcp216.utils.ComputeUtils.add;
import static org.homework.rcp216.utils.ComputeUtils.multiply;

/**
 * Réprente une association issu du fichier OpenOffice
 */
public class Association {

    private static final Logger LOGGER = LoggerFactory.getLogger(Association.class);
    Integer id;
    String categorie;
    String objet;
    String name;

    public Association() {
    }

    public Association(Integer id, String categorie, String objet, String name) {
        this.id = id;
        this.categorie = categorie;
        this.objet = objet;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public boolean isNotEmpty() {
        return (categorie != null && categorie.length() > 0) ||
                (objet != null && objet.length() > 0);
    }

    @Override
    public String toString() {
        return "Association{" +
                "id='" + id + '\'' +
                "categorie='" + categorie + '\'' +
                ", objet='" + objet + '\'' +
                '}';
    }

    /**
     * Crée des pairs avec en clef l'id de l'association et en valeur le mot
     *
     * @return
     */
    private static PairFunction<Row, Integer, String> pairByIdAssoc() {
        return (PairFunction<Row, Integer, String>) row -> new Tuple2<>(row.getInt(0), row.getString(1));
    }

    /**
     * Calcul le représentation vectorielle d'un ensemble de mot.
     *
     * @param word2VecRepo - Modèle Word2Vec permettant la rechercher de la représentation vectorielle
     * @return
     */
    private static Function<Tuple2<Integer, Iterable<String>>, Row> computeW2VForAssoc(JavaSparkContext sparkContext, final Word2VecSimpleRepo word2VecRepo) {
        Broadcast<Word2VecSimpleRepo> broadcast = sparkContext.broadcast(word2VecRepo);
        return (Function<Tuple2<Integer, Iterable<String>>, Row>) (Tuple2<Integer, Iterable<String>> tuple) -> {
            DenseVector sumVector = null;
            int cpt = 0;
            for (String value : tuple._2()) {
                DenseVector vector = broadcast.getValue().repo.get(value);
                if (vector == null) {
                    continue;
                }
                if (cpt == 0) {
                    sumVector = vector.copy();
                } else {
                    sumVector = add(sumVector, vector);
                }
                cpt++;
            }
            if (cpt == 0) {
                return RowFactory.create(tuple._1(), null);
            }
            return RowFactory.create(tuple._1(), multiply(sumVector, (1 / (double) cpt)));
        };
    }

    /**
     * <p>
     *     Convertie un dataset de type Association en Dataset Association + une colonne features correspondante à la représentation vectorielle issu de l'approche Word2Vec
     * </p>
     * <p>
     *     Approche incrémentale, beaucoup moins consommatrice en mémoire
     * </p>
     *
     * @param sparkContext
     * @param session
     * @param associationDataset de type Association
     * @param model - Modèle Word2Vec
     * @return categorie|id|name|objet|features
     */
    public static Dataset<Row> toWord2VecOptimized(JavaSparkContext sparkContext, SparkSession session, Dataset<Association> associationDataset, Broadcast<Word2VecModel> model, DecomposerByLemmaPos decomposerByLemmaPos) {
        final Map<String, DenseVector> word2VecRepo = Maps.newHashMap();
        model.getValue().getVectors().collectAsList().forEach((row) -> {
            word2VecRepo.put(row.getString(0), (DenseVector) row.get(1));
        });
        Word2VecSimpleRepo word2VecSimpleRepo = new Word2VecSimpleRepo();
        word2VecSimpleRepo.repo = word2VecRepo;
        Dataset<Row> wordsSet;
        if (decomposerByLemmaPos != null) {
            wordsSet = splitAssocationByLemmaPos(sparkContext, session, associationDataset, decomposerByLemmaPos);
        } else {
            wordsSet = splitAssociationByWords(sparkContext, session, associationDataset);
        }

        JavaRDD<Row> javaRDD = wordsSet
                .toJavaRDD()
                .mapToPair(pairByIdAssoc())
                .groupByKey()
                .map(computeW2VForAssoc(sparkContext, word2VecSimpleRepo));
        Dataset<Row> associationWord2Vec = session.createDataFrame(javaRDD,
                new StructType()
                        .add("idAss", DataTypes.IntegerType)
                        .add("features", new VectorUDT()));
        return associationDataset.join(associationWord2Vec, col("id").equalTo(col("idAss")))
                .drop(col("idAss"));
    }

    /**
     * Approche ensembliste de la problématique.
     * <p>Pose des problèmes de consommation mémoire</p>
     *
     * @param sparkContext
     * @param session
     * @param associationDataset
     * @param model
     * @return Retourne une représentation vectorielle de chacune des associations. 2 colonnes : <ul><li>id : id de l'association</li><li>features : représentation vectorielle de l'association</li></ul>
     */
    public static Dataset<Row> toWord2Vec(JavaSparkContext sparkContext, SparkSession session,
                                          Dataset<Association> associationDataset, Broadcast<Word2VecModel> model) {
        return splitAssociationByWords(sparkContext, session, associationDataset)
                .join(model.getValue().getVectors(), col("mot")
                        .equalTo(col("word")))
                .drop(col("mot"))
                .filter(col("vector").isNotNull())
                .groupBy(col("id"))
                .agg(expr("meanVector(vector) as features"));
    }

    /**
     * Méthode permettant de découper la description de chacune des associations en un ensemble de lignes de lemme associé à la position grammaticale pour chaque {@link Association}
     *
     * @param sparkContext
     * @param session
     * @param associationDataset
     * @return {@link Dataset} de type {@link Row}  avec 2 colonnes : colonnes id, identifiant de l'association, et lemma_pos, un lemme présent dans le descriptif de l'association.
     */
    public static Dataset<Row> splitAssocationByLemmaPos(JavaSparkContext sparkContext, SparkSession session,
                                                         Dataset<Association> associationDataset, DecomposerByLemmaPos decomposer) {
        JavaRDD<Row> rddAssoWord;
        rddAssoWord = associationDataset
                .toJavaRDD()
                .flatMapToPair(decomposer)
                .map((Function<Tuple2<Integer, String>, Row>) tuple -> RowFactory.create(tuple._1(), tuple._2()));
        return session.createDataFrame(rddAssoWord,
                new StructType()
                        .add("id", DataTypes.IntegerType)
                        .add("mot", DataTypes.StringType));
    }


    /**
     * Méthode permettant de découper la description de chacune des associations en un ensemble de lignes de mot pour chaque {@link Association}
     *
     * @param sparkContext
     * @param session
     * @param associationDataset
     * @return {@link Dataset} de type {@link Row}  avec 2 colonnes : colonnes id, identifiant de l'association, et mot, un mot présent dans le descriptif de l'association.
     */
    public static Dataset<Row> splitAssociationByWords(JavaSparkContext sparkContext, SparkSession session, Dataset<Association> associationDataset) {
        JavaRDD<Row> rddAssoWord;
        rddAssoWord = associationDataset
                .toJavaRDD()
                .flatMapToPair(DecomposerByWord.builder()
                        .broadcastStopWord(sparkContext.broadcast(StopWordsUtils.getStopWord())).build())
                .map((Function<Tuple2<Integer, String>, Row>) tuple -> RowFactory.create(tuple._1(), tuple._2()));

        return session.createDataFrame(rddAssoWord,
                new StructType()
                        .add("id", DataTypes.IntegerType)
                        .add("mot", DataTypes.StringType));
    }
}
