package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.model.Association;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import static org.apache.spark.sql.functions.desc;

public class Part1ExtractCategorie {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("document.url"))
                    .forceRefrestIfExist(false)
                    .sparkContext(sparkContext)
                    .sparkSession(session).load();

            Dataset<Row> rowDataset = associationDataset.groupBy("categorie").count().orderBy(desc("count"));
            rowDataset.repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter","|")
                    .mode(SaveMode.Overwrite)
                    .save(new File(properties.getProperty("workdir"),"resume").getCanonicalPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
