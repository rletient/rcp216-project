package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.storage.StorageLevel;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.loader.word2vec.Word2VecModelLoader;
import org.homework.rcp216.meta.MetaProgrammation;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.udf.VectorMeanUDAF;
import org.homework.rcp216.udf.W2VToWord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.expr;

public class Part6AssociationClassification {
    private static final Logger LOGGER = LoggerFactory.getLogger(Part6AssociationClassification.class);

    private static final String NAME = "Part6AssociationClassification";

    public static File getPathInWorkdir(Properties properties, String name) throws IOException {
        return new File(properties.getProperty("workdir"), name);
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            // Chargement du model Word2Vec
            Broadcast<Word2VecModel> model = Word2VecModelLoader.create()
                    .sparkContext(sparkContext)
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("word2vec.model.url"))
                    .load();

            // Enregistrement des fonctions spark maison
            session.udf().register("meanVector", VectorMeanUDAF.createMeanVectorUDAF());
            session.udf().register("w2v2words", new W2VToWord(model), DataTypes.StringType);

            File fDataset = getPathInWorkdir(properties, NAME);
            Dataset<Row> datasetIdAssocAndMeanVector = null;
            if (fDataset.exists()) {
                datasetIdAssocAndMeanVector = session.read().parquet(fDataset.getCanonicalPath());
            } else {

                // Chargement du fichier des associations sous la forme d'un Dataset
                Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                        .workdir(new File(properties.getProperty("workdir")))
                        .url(properties.getProperty("document.url"))
                        .forceRefrestIfExist(false)
                        .sparkContext(sparkContext)
                        .sparkSession(session).load();

                // Création de l'instance permettant la décomposition de phrases en lemme + pos
                DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                        .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                        .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                        .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                        .build();

                // Création d'un dataset id association | moyenne des vecteurs des mots relatifs à l'association
                datasetIdAssocAndMeanVector = Association.toWord2VecOptimized(sparkContext, session, associationDataset, model, decomposerByLemmaPos).persist(StorageLevel.MEMORY_ONLY_SER());

                // Sauvegarde du modèle
                datasetIdAssocAndMeanVector.write().mode(SaveMode.Overwrite).save(fDataset.getCanonicalPath());
            }

            // Répartition des associations dans le cluster
            File fClustered = getPathInWorkdir(properties, NAME + ".cluster");
            Dataset<Row> clustered = null;
            if (fClustered.exists()) {
                clustered = session.read().parquet(fClustered.getCanonicalPath());
            } else {
                KMeansModel kmeansModel = new KMeans().setK(41).setMaxIter(200).setSeed(1L).fit(datasetIdAssocAndMeanVector);
                // categorie | id | name | objet | w2v | clusterId
                clustered = kmeansModel.transform(datasetIdAssocAndMeanVector)
                        .withColumnRenamed("prediction", "clusterId")
                        .withColumnRenamed("features", "w2v");
                clustered.write().mode(SaveMode.Overwrite).save(getPathInWorkdir(properties, NAME + ".cluster").getCanonicalPath());
            }

            // Création d'un dataset avec par id de cluster les mots word2vec associés
            // clusterIdW2V | clusterW2Vec
            Dataset<Row> clusterW2VDataset = clustered.groupBy(col("clusterId"))
                    .agg(expr("meanVector(w2v) as clusterW2V"))
                    .withColumn("clusterW2Vec", callUDF("w2v2words", col("clusterW2V")))
                    .withColumnRenamed("clusterId", "clusterIdW2V")
                    .drop(col("w2v"))
                    .drop(col("clusterW2V"));

            clustered.join(clusterW2VDataset,col("clusterId").equalTo(col("clusterIdW2V")))
                    .select(col("id"), col("categorie"), col("clusterId"), col("clusterW2Vec"))
                    .repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter", "|")
                    .mode(SaveMode.Overwrite)
                    .save(getPathInWorkdir(properties, NAME + ".csv").getCanonicalPath());

            clustered.join(clusterW2VDataset,col("clusterId").equalTo(col("clusterIdW2V")))
                    .select(col("id"), col("categorie"), col("clusterId"), col("objet"))
                    .repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter", "|")
                    .mode(SaveMode.Overwrite)
                    .save(getPathInWorkdir(properties, NAME + ".objet.csv").getCanonicalPath());



        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

