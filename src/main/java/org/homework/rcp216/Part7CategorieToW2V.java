package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.storage.StorageLevel;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.loader.word2vec.Word2VecModelLoader;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.udf.VectorMeanUDAF;
import org.homework.rcp216.udf.W2VToWord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.apache.spark.sql.functions.*;

public class Part7CategorieToW2V {
    private static final Logger LOGGER = LoggerFactory.getLogger(Part7CategorieToW2V.class);

    private static final String NAME = "Part7CategorieToW2V";

    public static File getPathInWorkdir(Properties properties, String name) throws IOException {
        return new File(properties.getProperty("workdir"), name);
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            // Chargement du model Word2Vec
            Broadcast<Word2VecModel> model = Word2VecModelLoader.create()
                    .sparkContext(sparkContext)
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("word2vec.model.url"))
                    .load();

            // Enregistrement des fonctions spark maison
            session.udf().register("meanVector", VectorMeanUDAF.createMeanVectorUDAF());
            session.udf().register("w2v2words", new W2VToWord(model), DataTypes.StringType);

            File fDataset = getPathInWorkdir(properties, NAME);
            Dataset<Row> associationW2VDataset = null;
            if (fDataset.exists()) {
                associationW2VDataset = session.read().parquet(fDataset.getCanonicalPath());
            } else {

                // Chargement du fichier des associations sous la forme d'un Dataset
                Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                        .workdir(new File(properties.getProperty("workdir")))
                        .url(properties.getProperty("document.url"))
                        .forceRefrestIfExist(false)
                        .sparkContext(sparkContext)
                        .sparkSession(session).load();

                // Création de l'instance permettant la décomposition de phrases en lemme + pos
                DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                        .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                        .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                        .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                        .build();

                // categorie|id|name|objet|features
                associationW2VDataset = Association.toWord2VecOptimized(sparkContext, session, associationDataset, model, decomposerByLemmaPos).persist(StorageLevel.MEMORY_ONLY_SER());


                // Sauvegarde du modèle
                associationW2VDataset.write().mode(SaveMode.Overwrite).save(fDataset.getCanonicalPath());
            }

            // Création d'un dataset avec par id de cluster les mots word2vec associés
            // clusterIdW2V | clusterW2Vec
            Dataset<Row> rows = associationW2VDataset.groupBy(col("categorie"))
                    .agg(expr("meanVector(features) as categorieW2V"))
                    .withColumn("categorieW2Vec", callUDF("w2v2words", col("categorieW2V")))
                    .select(col("categorie"), col("categorieW2Vec"));

            rows.show();

            rows.repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter", "|")
                    .mode(SaveMode.Overwrite)
                    .save(getPathInWorkdir(properties, NAME + ".csv").getCanonicalPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

