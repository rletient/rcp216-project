package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.feature.PCA;
import org.apache.spark.ml.feature.PCAModel;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.loader.word2vec.Word2VecModelLoader;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.spark.VectorToFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.desc;

public class PartBonusACPOnW2V {

    private final static Logger LOGGER = LoggerFactory.getLogger(PartBonusACPOnW2V.class);

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("document.url"))
                    .forceRefrestIfExist(false)
                    .sparkContext(sparkContext)
                    .sparkSession(session).load();


            LOGGER.info("Chargement du model Word2Vec");
            Broadcast<Word2VecModel> model = Word2VecModelLoader.create()
                    .sparkContext(sparkContext)
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("word2vec.model.url"))
                    .load();


            DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                    .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                    .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                    .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                    .build();
            Dataset<Row> datasetIdAssocAndMeanVector = Association.toWord2VecOptimized(sparkContext, session, associationDataset, model, decomposerByLemmaPos);

            LOGGER.info("Taille du dataset : " + datasetIdAssocAndMeanVector.count());
            // Pas besoin de normaliser le modèle, toutes les données sont homogènes
            PCAModel pcaModel = new PCA().setInputCol("features").setOutputCol("pcaFeatures").setK(3).fit(datasetIdAssocAndMeanVector);
            Dataset<Row> datasetAfterPCA = pcaModel.transform(datasetIdAssocAndMeanVector);

            LOGGER.info("acpOnAssociationWord2Vec - sauvegarde au format csv");
            VectorToFile vtf = VectorToFile.writeTo(properties.getProperty("output.acp.csv")).headers("Dim.1", "Dim.2", "Dim.3");
            datasetAfterPCA.select(col("pcaFeatures")).toJavaRDD().collect().forEach(vtf::print);
            vtf.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
