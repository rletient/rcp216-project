package org.homework.rcp216;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.evaluation.ClusteringEvaluator;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.storage.StorageLevel;
import org.homework.rcp216.loader.data.AssociationDatasetLoader;
import org.homework.rcp216.loader.word2vec.Word2VecModelLoader;
import org.homework.rcp216.model.Association;
import org.homework.rcp216.opennlp.DecomposerByLemmaPos;
import org.homework.rcp216.udf.ConcatStringUDAF;
import org.homework.rcp216.udf.W2VToWord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.expr;

public class Part4AssociationToW2Vec {
    private static final Logger LOGGER = LoggerFactory.getLogger(Part4AssociationToW2Vec.class);

    private static final String NAME = "Part4AssociationToW2Vec";

    public static File getPathInWorkdir(Properties properties, String name) throws IOException {
        return new File(properties.getProperty("workdir"), name);
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));

            SparkConf conf = new SparkConf().setMaster("local").setAppName("project-rcp216");
            conf.set("executor-memory", "7g");
            conf.set("spark.sql.shuffle.partitions","4");
            JavaSparkContext sparkContext = new JavaSparkContext(conf);
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            // Chargement du fichier des associations sous la forme d'un Dataset
            Dataset<Association> associationDataset = AssociationDatasetLoader.builder()
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("document.url"))
                    .forceRefrestIfExist(false)
                    .sparkContext(sparkContext)
                    .sparkSession(session).load();

            // Création de l'instance permettant la décomposition de phrases en lemme + pos
            DecomposerByLemmaPos decomposerByLemmaPos = DecomposerByLemmaPos.builder()
                    .posModelLocation(new File(properties.getProperty("opennlp.postmodel.path")))
                    .tokenModelLocation(new File(properties.getProperty("opennlp.tokenmodel.path")))
                    .lemmaDictionnaryLocation(new File(properties.getProperty("opennlp.lemmadictionnary.path")))
                    .build();

            // Chargement du model Word2Vec
            Broadcast<Word2VecModel> model = Word2VecModelLoader.create()
                    .sparkContext(sparkContext)
                    .workdir(new File(properties.getProperty("workdir")))
                    .url(properties.getProperty("word2vec.model.url"))
                    .load();

            // Création d'un dataset id association | moyenne des vecteurs des mots relatifs à l'association
            // id | feature
            Dataset<Row> datasetIdAssocAndMeanVector = Association.toWord2VecOptimized(sparkContext, session, associationDataset, model, decomposerByLemmaPos);

            // Enregistrement de la fonction de conversion d'un vector word2vec vers une liste de mot
            session.udf().register("w2v2words", new W2VToWord(model), DataTypes.StringType);

            // Remplacement des vectors calculés (moyenne) par des mots représentatifs de ces vectors
            Dataset<Row> datasetIdAndW2VWords = datasetIdAssocAndMeanVector
                    .withColumn("w2vwords", callUDF("w2v2words", col("features")))
                    .drop(col("features"));


            session.udf().register("concat", ConcatStringUDAF.createConcatStringUDAF());

            // id | mot
            Dataset<Row> idAndLemmaPosWordsDataset = Association.splitAssocationByLemmaPos(sparkContext, session, associationDataset, decomposerByLemmaPos)
                    .groupBy(col("id"))
                    .agg(expr(("concat(mot) as mots")))
                    .withColumnRenamed("id", "idSplit");

            // Sauvegarde du modèle au format csv
            datasetIdAndW2VWords.join(idAndLemmaPosWordsDataset, col("idSplit").equalTo(col("id")))
                    .drop(col("idSplit"))
                    .repartition(1).write().format("csv")
                    .option("header", "true")
                    .option("delimiter", "|")
                    .mode(SaveMode.Overwrite)
                    .save(getPathInWorkdir(properties, NAME).getCanonicalPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

